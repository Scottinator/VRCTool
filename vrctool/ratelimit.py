import datetime
import json
import math
import time
from enum import Enum
from pathlib import Path

import requests

from vrctool.logging import getLogger #isort: skip
log = getLogger(__name__)


RATELIMIT_JSON = Path('.ratelimits')

class RateLimiterType(Enum):
    EVENT_QUEUE      = 'EQ'
    TIME_DELAY       = 'TD'
    GITHUB           = 'GH'
    NOT_RATE_LIMITED = '-'

class RateLimiterManager:
    _TYPES = {}
    _VERSION = 1
    _INSTANCE = None

    @classmethod
    def getInstance(cls) -> 'RateLimiterManager':
        if not cls._INSTANCE:
            cls._INSTANCE = cls()
            cls._INSTANCE.Load()
        return cls._INSTANCE

    def __init__(self) -> None:
        self.ratelimiters = {}

    @classmethod
    def RegisterType(cls, rlcls) -> None:
        cls._TYPES[rlcls.TYPE_ID] = rlcls


    def Load(self) -> None:
        if RATELIMIT_JSON.is_file():
            with RATELIMIT_JSON.open('r') as f:
                container = json.load(f)
                if container['version'] == self._VERSION:
                    for rldata in container['data']:
                        rl = self._TYPES[rldata['type']]()
                        rl.deserialize(rldata)
                        self.ratelimiters[rl.instanceID] = rl

    def Save(self) -> None:
        data = {
            'version': 1,
            'data': [x.serialize() for x in self.ratelimiters.values()]
        }
        with RATELIMIT_JSON.open('w') as f:
            json.dump(data, f)

    def getOrCreate(self, typeid, instanceid, *args, **kwargs) -> 'BaseRateLimiter':
        if isinstance(typeid, RateLimiterType):
            typeid = typeid.value
        if instanceid not in self.ratelimiters:
            rl = self._TYPES[typeid](instanceid, *args, **kwargs)
            self.ratelimiters[rl.instanceID] = rl
            return rl
        else:
            return self.ratelimiters[instanceid]

def getOrCreate(typeid, instanceid, *args, **kwargs) -> 'BaseRateLimiter':
    return RateLimiterManager.getInstance().getOrCreate(typeid, instanceid, *args, **kwargs)

class RequestCounter:
    def __init__(self, periodInSeconds: int) -> None:
        self.requestTimes = []
        self.periodInSeconds = periodInSeconds

    def pushEvent(self) -> None:
        self._gc()
        self.requestTimes.append(time.time())

    def _gc(self) -> None:
        end = time.time()
        self.requestTimes = [e for e in self.requestTimes if e > (end-self.periodInSeconds)]

    def count(self) -> int:
        self._gc()
        return len(self.requestTimes)

class BaseRateLimiter:
    TYPE_ID = ''
    def __init__(self, instanceID: str = None) -> None:
        self.instanceID: str = instanceID

    def pushEvent(self) -> None:
        return

    def isRateLimited(self) -> None:
        return

    def serialize(self) -> dict:
        return {
            'type': self.TYPE_ID,
            'iid': self.instanceID,
        }
    def deserialize(self, data: dict) -> None:
        assert data['type'] == self.TYPE_ID
        self.instanceID = data['iid']


class _EventQueueRateLimiter(BaseRateLimiter):
    TYPE_ID = 'EQ'
    def __init__(self, instanceID: str = None, reqCount: int = None, periodInSeconds: int = None) -> None:
        super().__init__(instanceID)
        self.requestTimes = []
        self.periodInSeconds = periodInSeconds or 3600 # 1hr
        self.maxRequests: int = reqCount or 60 # 1 per minute

    def pushEvent(self) -> None:
        self._gc()
        self.requestTimes.append(time.time())
        RateLimiterManager.getInstance().Save()

    def _gc(self) -> None:
        end = time.time()
        self.requestTimes = [e for e in self.requestTimes if e > (end-self.periodInSeconds)]

    def count(self) -> int:
        self._gc()
        #RateLimiterManager.getInstance().Save()
        return len(self.requestTimes)

    def isRateLimited(self) -> bool:
        return self.count() >= self.maxRequests

    def delayUntilUsable(self) -> None:
        #format='%(asctime)s [%(levelname)-8s]: %(message)s',
        #datefmt='%m/%d/%Y %I:%M:%S %p',

        if not self.isRateLimited():
            log.info(f'[{self.instanceID}] Not currently rate-limited. [{self.count()} req/{self.maxRequests} max/{self.periodInSeconds}s]')
            return
        i = 0
        start = time.time()
        while True:
            end = time.time()
            deltaS = round(end - start)
            logtime = datetime.datetime.now().strftime('%m/%d/%Y %I:%M:%S %p') # DO NOT ADD TIMEZONE
            prefix = f'{logtime} [WARNING ]:'
            if self.isRateLimited():
                i=(i+1)%3
                dot = ''.join([('.' if i==j else ' ') for j in range(3)])
                print(f'{prefix} [{self.instanceID}] RATE LIMIT REACHED. Waiting for unblock{dot} [{self.count()} req/{self.maxRequests} max/{self.periodInSeconds}s] [{deltaS}s]', end='\r')
                time.sleep(1)
            else:
                log.warning(f'[{self.instanceID}] RATE LIMIT REACHED. Block lifted after {deltaS}s...')
                return

    def serialize(self) -> dict:
        data = super().serialize()
        data['q'] = self.requestTimes
        #data['T'] = self.periodInSeconds
        #data['N'] = self.maxRequests
        return data
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
        self.requestTimes = [float(x) for x in data['q']]
        #self.periodInSeconds = int(data['T'])
        #self.maxRequests = int(data['N'])
RateLimiterManager.RegisterType(_EventQueueRateLimiter)

class _NotLimitedRateLimiter(BaseRateLimiter):
    TYPE_ID = '-'
    def __init__(self, instanceID: str = None) -> None:
        super().__init__(instanceID)

    def pushEvent(self) -> None:
        return

    def isRateLimited(self) -> bool:
        return False

    def delayUntilUsable(self) -> None:
        log.info(f'[{self.instanceID}] Not currently rate-limited.')
        return

    def serialize(self) -> dict:
        return super().serialize()

    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
RateLimiterManager.RegisterType(_NotLimitedRateLimiter)

class _TimeDelayRateLimiter(BaseRateLimiter):
    TYPE_ID = 'TD'
    def __init__(self, instanceID: str = None, reqCount: int = None, periodInSeconds: int = None) -> None:
        super().__init__(instanceID)
        self.lockedAt: float = -1
        self.unlockAt: float = -1
        self.periodInSeconds: int = periodInSeconds or 3600 # 1hr
        self.maxRequests: int = reqCount or 60 # 1 per minute
        self.delayPerRequest = float(self.periodInSeconds) / float(self.maxRequests)

    def pushEvent(self) -> None:
        self.lockedAt = time.time()
        self.unlockAt = self.lockedAt + self.delayPerRequest
        RateLimiterManager.getInstance().Save()

    def isRateLimited(self) -> bool:
        if self.unlockAt == -1 or self.lockedAt == -1:
            return False
        return time.time() <= self.unlockAt

    def delayUntilUsable(self) -> None:
        if not self.isRateLimited():
            print('Not currently rate-limited.')
            return
        i = 0
        start = time.time()
        unlocksInS = round(self.unlockAt - self.lockedAt)
        while True:
            end = time.time()
            deltaS = round(end - start)
            logtime = datetime.datetime.now().strftime('%m/%d/%Y %I:%M:%S %p') # DO NOT ADD TIMEZONE
            prefix = f'{logtime} [WARNING ]:'
            if self.isRateLimited():
                i=(i+1)%3
                dot = ''.join([('.' if i==j else ' ') for j in range(3)])
                print(f'{prefix} RATE LIMIT REACHED. Waiting for unblock{dot} [{self.maxRequests} req/{self.periodInSeconds}s] [{deltaS}/{unlocksInS}s]', end='\r')
                time.sleep(1)
            else:
                log.warning(f'RATE LIMIT REACHED. Block lifted after {deltaS}s...')
                return

    def serialize(self) -> dict:
        data = super().serialize()
        data['s'] = self.lockedAt
        return data
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
        self.lockedAt = float(data['s'])
        self.unlockAt = self.lockedAt + self.delayPerRequest
RateLimiterManager.RegisterType(_TimeDelayRateLimiter)

class _GitHubRateLimiter(BaseRateLimiter):
    TYPE_ID = 'GH'
    def __init__(self, instanceID: str = None, rscid: str = '', data: dict={}) -> None:
        super().__init__(instanceID)
        self.requestTimes = []
        self.resourceID: str = ''
        self.maxRequests: int = 60
        self.remainingRequests: int = 60
        self.resetsAt = datetime.datetime.now(tz=datetime.timezone.utc)
        self._hasFetchedRateLimits = False

    def parseGHRateLimits(self, data):
        #print(repr(data))
        self.maxRequests: int = data.get('limit', 60)
        self.remainingRequests: int = data.get('remaining', 60)
        default = datetime.datetime.now(tz=datetime.timezone.utc).timestamp()
        self.resetsAt = datetime.datetime.fromtimestamp(data.get('reset', default), tz=datetime.timezone.utc)
        #print(f'{self.resourceID} - {self.remainingRequests}/{self.maxRequests} reset@ {self.resetsAt}')
        self._hasFetchedRateLimits = True

    def _fetchCurrentLimits(self, reset: bool=False) -> None:
        if self._hasFetchedRateLimits or reset:
            return
        req = requests.get('https://api.github.com/rate_limit')
        req.raise_for_status()
        data = req.json()['resources'][self.resourceID]
        print(repr(data))

        self.maxRequests: int = data.get('limit', 60)
        self.remainingRequests: int = data.get('remaining', 60)
        default = datetime.datetime.now(tz=datetime.timezone.utc).timestamp()
        self.resetsAt = datetime.datetime.fromtimestamp(data.get('reset', default), tz=datetime.timezone.utc)
        self._hasFetchedRateLimits = True

    def pushEvent(self) -> None:
        self.remainingRequests -= 1
        RateLimiterManager.getInstance().Save()

    def processResponseHeaders(self, res) -> None:
        self.maxRequests = int(res.headers.get('X-RateLimit-Limit', self.maxRequests))
        self.remainingRequests = int(res.headers.get('X-RateLimit-Remaining', self.remainingRequests-1))
        self.resetsAt = datetime.datetime.fromtimestamp(res.headers.get('X-RateLimit-Reset', self.resetsAt), tz=datetime.timezone.utc)
        RateLimiterManager.getInstance().Save()

    def isRateLimited(self) -> bool:
        self._fetchCurrentLimits()
        if self.remainingRequests >= self.maxRequests:
            if datetime.datetime.now(tz=datetime.timezone.utc) < self.resetsAt:
                self._fetchCurrentLimits(reset=True)
                return False
            return True
        return False

    def delayUntilUsable(self) -> None:
        if not self.isRateLimited():
            log.info(f'[{self.instanceID}] Not currently rate-limited. [{self.maxRequests-self.remainingRequests} req/{self.maxRequests} max]')
            return
        i = 0
        start = time.time()
        while True:
            end = time.time()
            deltaS = round(end - start)
            logtime = datetime.datetime.now().strftime('%m/%d/%Y %I:%M:%S %p') # DO NOT ADD TIMEZONE
            prefix = f'{logtime} [WARNING ]:'
            if self.isRateLimited():
                i=(i+1)%3
                dot = ''.join([('.' if i==j else ' ') for j in range(3)])
                print(f'{prefix} [{self.instanceID}] RATE LIMIT REACHED. Waiting for unblock{dot} [{self.maxRequests-self.remainingRequests} req/{self.maxRequests} max] [{deltaS}s]', end='\r')
                time.sleep(1)
            else:
                log.warning(f'[{self.instanceID}] RATE LIMIT REACHED. Block lifted after {deltaS}s...')
                return

    def serialize(self) -> dict:
        data = super().serialize()
        return data
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
RateLimiterManager.RegisterType(_GitHubRateLimiter)
