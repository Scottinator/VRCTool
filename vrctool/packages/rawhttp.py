import datetime
from pathlib import Path, PurePosixPath
from typing import Dict, List, Optional
from vrctool import http
from .base import EPackageType, Package, PackageVersion, packagetype
from vrctool.ratelimit import BaseRateLimiter, RateLimiterManager, RateLimiterType
import requests
import json

from vrctool.utils import getFilenameFromURL
from vrctool.vrcmgapi import EVRCMGModType, VRCMGAPI, VRCMGMod, VRCMGModVersion

@packagetype
class RawHTTPPackage(Package):
    TYPEID = 'http'

    def __init__(self, _id: str = None, typ: EPackageType = None, url: str = None) -> None:
        super().__init__(_id, typ)
        self.url = url

    def copyFromOld(self, other: Package) -> None:
        super().copyFromOld(other)
        self.url = getattr(other, 'url', self.url)

    def deserialize(self, data, skip_installation: bool = False) -> None:
        super().deserialize(data, skip_installation)
        self.url = data['url']

    def serialize(self) -> dict:
        data = super().serialize()
        data['url'] = self.url
        return data

    def fetchLatestRelease(self, cache=False) -> Optional[PackageVersion]:
        pv = PackageVersion(self)
        pv.version = '???'
        pv.date = datetime.datetime.now(tz=datetime.timezone.utc)
        pv.files = {}
        pv.addFile(self.url, str(self.getSubDir() / getFilenameFromURL(self.url)))
        return pv

    def fetchLatestVersionInto(self, version: PackageVersion, for_review: bool = False) -> None:
        #version.for_game_build = CURRENT_GAME_BUILD
        version.date = datetime.datetime.now(tz=datetime.timezone.utc)
        version.files = {}
        pf = version.addFile(self.url, str(self.getSubDir() / getFilenameFromURL(self.url)))
