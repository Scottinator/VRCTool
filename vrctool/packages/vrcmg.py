import datetime
from pathlib import Path, PurePosixPath
from typing import Dict, List, Optional
from vrctool import http
from .base import EPackageType, Package, PackageVersion, packagetype
from vrctool.ratelimit import BaseRateLimiter, RateLimiterManager, RateLimiterType
import requests
import json

from vrctool.utils import getFilenameFromURL
from vrctool.vrcmgapi import EVRCMGModType, VRCMGAPI, VRCMGMod, VRCMGModVersion
@packagetype
class VRCMGPackage(Package):
    TYPEID = 'vrcmg'
    API: VRCMGAPI = None
    def __init__(self, _id: str=None, typ: EPackageType=None, apiID: int=0, filenames: List[str]=None, allow_prerelease:bool = None) -> None:
        super().__init__(_id, typ)
        self.apiID: int = apiID
        self.filenames = filenames
        self.allow_prerelease = allow_prerelease

    def copyFromOld(self, other: Package) -> None:
        super().copyFromOld(other)
        self.apiID = getattr(other, 'apiID', self.apiID)
        self.filenames = getattr(other, 'filenames', self.filenames)
        self.allow_prerelease = getattr(other, 'allow_prerelease', self.allow_prerelease)

    def deserialize(self, data, skip_installation: bool = False) -> None:
        super().deserialize(data, skip_installation)
        self.apiID = data['api-id']
        if 'filenames' in data:
            self.filenames = data['filenames']
        elif 'filename' in data:
            self.filenames = [data['filename']]
        self.allow_prerelease = data.get('allow_prerelease',False)
        
    def serialize(self) -> dict:
        data = super().serialize()
        data['api-id'] = self.apiID
        data['filenames'] = self.filenames
        data['allow_prerelease'] = self.allow_prerelease
        return data

    def _getAPI(self) -> VRCMGAPI:
        if self.API is None:
            self.API = VRCMGAPI()
        return self.API

    def fetchLatestRelease(self, cache=False) -> Optional[PackageVersion]:
        mod: VRCMGMod = self._getAPI().getModByID(self.apiID)
        v: VRCMGModVersion = mod.getLatestVersion()
        if not v:
            return None

        pkg: 'VRCMGPackage' = VRCMGPackage()
        pkg.id = mod.aliases[0].replace(' ', '').lower()
        pkg.name = mod.aliases[0]
        pkg.type = EPackageType.MOD if v.modType == EVRCMGModType.MOD else EPackageType.PLUGIN
        pkg.description = ''
        pkg.apiID = mod.id

        pv: PackageVersion = PackageVersion(pkg)
        pv.version = v.modVersion
        pv.changes = v.changelog
        pv.date = v.updateDate
        pv.files = {}
        subdir = self.getSubDir()
        pv.addFile(v.downloadLink, str(subdir / getFilenameFromURL(v.downloadLink)))
        if len(pv.files):
            return pv
        return None
