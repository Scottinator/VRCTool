import datetime
from pathlib import Path, PurePosixPath
from typing import Dict, List, Optional
from vrctool import http
from .base import EPackageType, Package, PackageFile, PackageVersion, packagetype
from vrctool.ratelimit import BaseRateLimiter, RateLimiterManager, RateLimiterType
import requests
import shutil

from vrctool.utils import getFilenameFromURL
from vrctool.vrcmgapi import EVRCMGModType, VRCMGAPI, VRCMGMod, VRCMGModVersion

from vrctool.logging import PostMessagingBox, getLogger  # isort: skip
log = getLogger(__name__)

@packagetype
class FilesystemPackage(Package):
    TYPEID = 'file'

    def __init__(self, _id: str = None, typ: EPackageType = None, path: str = None) -> None:
        super().__init__(_id, typ)
        self.path = path

    def copyFromOld(self, other: Package) -> None:
        super().copyFromOld(other)
        self.path = getattr(other, 'path', self.path)

    def deserialize(self, data, skip_installation: bool = False) -> None:
        super().deserialize(data, skip_installation)
        self.path = data['source-file']
        self.versions = {}
        pv = PackageVersion(self)
        pv.date = datetime.datetime.now(tz=datetime.timezone.utc)
        pv.files['0'] = PackageFile(pv)
        self.versions['0'] = pv

    def serialize(self) -> dict:
        data = super().serialize()
        data['source-file'] = self.path
        return data

    def install(self, msgbox: PostMessagingBox, pv: PackageVersion, cwd: Path) -> None:
        source: Path = Path(self.path)
        filename = self.getInstallLocation(source.name)
        log.info(f'{self.path} -> {filename}')
        shutil.copy2(self.path, filename)
        self.KEEP.add(filename)
        self.installation.packaged_files.add(filename)

    def getLatestAvailableVersion(self, game_build: int) -> Optional[PackageVersion]:
        return self.versions['0']
    def getLatestVersion(self) -> Optional[PackageVersion]:
        return self.versions['0']

