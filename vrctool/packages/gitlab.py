import datetime
from pathlib import Path
from typing import List, Optional
from .base import Package, PackageVersion, packagetype, EPackageType

from urllib.parse import urlencode, urlparse
import json
import requests
import re

from vrctool import http
@packagetype
class GitLabPackage(Package):
    TYPEID = 'gitlab'

    def __init__(self, _id: str = None, typ: EPackageType = None, hostname: str = None, namespace: str = None, project: str = None, filenames: List[str] = None, allow_prerelease: bool = None) -> None:
        super().__init__(_id, typ)
        self.hostname = hostname
        self.namespace = namespace
        self.project = project
        self.filenames = filenames
        self.allow_prerelease = allow_prerelease

    def copyFromOld(self, other: Package) -> None:
        super().copyFromOld(other)
        self.hostname = getattr(other, 'hostname', self.hostname)
        self.namespace = getattr(other, 'namespace', self.namespace)
        self.project = getattr(other, 'project', self.project)
        self.filenames = getattr(other, 'filenames', self.filenames)
        self.allow_prerelease = getattr(other, 'allow_prerelease', self.allow_prerelease)

    def deserialize(self, data, skip_installation: bool = False) -> None:
        super().deserialize(data, skip_installation)
        self.hostname = data['hostname']
        self.namespace = data['namespace']
        self.project = data['project']
        if 'filenames' in data:
            self.filenames = data['filenames']
        elif 'filename' in data:
            self.filenames = [data['filename']]
        self.allow_prerelease = data.get('allow_prerelease', False)

    def serialize(self) -> dict:
        data = super().serialize()
        data['hostname'] = self.hostname
        data['namespace'] = self.namespace
        data['project'] = self.project
        data['filenames'] = self.filenames
        data['allow_prerelease'] = self.allow_prerelease
        return data

    def fetchLatestRelease(self, cache=False) -> Optional[PackageVersion]:
        repo_id = urlencode(f'{self.namespace}/{self.project}', safe='')
        uri = f'https://{self.hostname}/projects/{repo_id}/releases'
        data: dict
        if cache:
            cachePath = Path('.cache') / 'gitlab' / \
                self.namespace / self.project / 'releases.json'
            if not cachePath.parent.is_dir():
                cachePath.parent.mkdir(parents=True)
            self._last_request_hit_cache = not http.downloadToIfModified(uri, str(cachePath))
            with cachePath.open('r') as f:
                data = json.load(f)
        else:
            self._last_request_hit_cache = False
            res = requests.get(uri)
            res.raise_for_status()
            data = res.json()
        res = requests.get()
        res.raise_for_status()
        data = res.json()
        for release in data:
            version = PackageVersion(self)
            version.version = release['tag_name'] or release['name']
            version.changes = release['description']
            version.date = datetime.datetime.strptime(release['released_at'], "%Y-%m-%dT%H:%M:%SZ")
            version.date = version.date.replace(tzinfo=datetime.timezone.utc)
            version.files = {}
            for asset in release['assets']:
                for link in asset['links']:
                    for filename in self.filenames:
                        if re.match(filename, link['name']) is not None:
                            subdir = self.getSubDir()
                            pf = version.addFile(
                                link['url'], str(subdir / link['name']))
            if len(version.files):
                return version
        return None
