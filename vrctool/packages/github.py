import datetime
from pathlib import Path
from typing import Dict, List, Optional
from vrctool import http
from .base import EPackageType, Package, PackageVersion, packagetype
from vrctool.ratelimit import BaseRateLimiter, RateLimiterManager, RateLimiterType
import requests
import json

@packagetype
class GitHubPackage(Package):
    TYPEID = 'github'
    LIMITERS: Dict[str, BaseRateLimiter] = None

    def __init__(self, _id: str = None, typ: EPackageType = None, namespace: str = None, project: str = None, filenames: List[str] = None, allow_prerelease: bool = None) -> None:
        super().__init__(_id, typ)
        self.namespace = namespace
        self.project = project
        self.filenames: List[str] = filenames
        self.allow_prerelease = allow_prerelease

    def copyFromOld(self, other: Package) -> None:
        super().copyFromOld(other)
        self.namespace = getattr(other, 'namespace', self.namespace)
        self.project = getattr(other, 'project', self.project)
        self.filenames = getattr(other, 'filenames', self.filenames)
        self.allow_prerelease = getattr(other, 'allow_prerelease', self.allow_prerelease)

    def deserialize(self, data, skip_installation: bool = False) -> None:
        super().deserialize(data, skip_installation)
        self.namespace = data['namespace']
        self.project = data['project']
        if 'filename' in data:
            self.filenames = [data['filename']]
        elif 'filenames' in data:
            self.filenames = data['filenames']
        self.allow_prerelease = data.get('allow_prerelease', False)

    def serialize(self) -> dict:
        data = super().serialize()
        data['namespace'] = self.namespace
        data['project'] = self.project
        data['filenames'] = self.filenames
        if self.allow_prerelease:
            data['allow_prerelease'] = self.allow_prerelease
        return data

    def getRateLimiter(self, uri: str) -> Optional[BaseRateLimiter]:
        if self.LIMITERS is None:
            req = requests.get('https://api.github.com/rate_limit')
            req.raise_for_status()
            self.LIMITERS = {}
            self.LIMITERS['raw'] = RateLimiterManager.getInstance().getOrCreate(
                RateLimiterType.NOT_RATE_LIMITED, 'gh-raw')
            data = req.json()
            for rscid, resource in data['resources'].items():
                self.LIMITERS[rscid] = lim = RateLimiterManager.getInstance().getOrCreate(RateLimiterType.GITHUB, 'gh-'+rscid)
                lim.resourceID = rscid
                lim.parseGHRateLimits(resource)
        if 'https://raw.githubusercontent.com/' in uri:
            return self.LIMITERS['raw']
        return self.LIMITERS['core']

    def isRateLimited(self, uri: str) -> bool:
        return self.getRateLimiter() and self.getRateLimiter().isLimited()

    def pushRateLimitedEvent(self, uri: str) -> bool:
        rl = self.getRateLimiter(uri)
        if rl:
            rl.pushEvent()

    def fetchLatestRelease(self, cache=False) -> Optional[PackageVersion]:
        data: dict
        uri = f'https://api.github.com/repos/{self.namespace}/{self.project}/releases'
        self.getRateLimiter(uri).delayUntilUsable()
        try:
            if cache:
                cachePath = Path('.cache') / 'github' / \
                    self.namespace / self.project / 'releases.json'
                if not cachePath.parent.is_dir():
                    cachePath.parent.mkdir(parents=True)
                self._last_request_hit_cache = not http.downloadToIfModified(uri, str(cachePath), params={
                    'per_page': 1000,  # default: 100
                })
                if cachePath.is_file():
                    with cachePath.open('r', encoding='utf-8') as f:
                        data = json.load(f)
                else:
                    self._last_request_hit_cache = False
                    res = requests.get(uri, params={
                        'per_page': 1000,  # default: 100
                    })
                    res.raise_for_status()
                    data = res.json()
            else:
                self._last_request_hit_cache = False
                res = requests.get(uri, params={
                    'per_page': 1000,  # default: 100
                })
                res.raise_for_status()
                data = res.json()
        finally:
            if not self._last_request_hit_cache:
                self.getRateLimiter(uri).pushEvent()
        #print(repr(data))
        latestPV = None
        latestPublish = None
        for release in data:
            if release['draft']:
                continue
            if release['prerelease'] and not self.allow_prerelease:
                continue
            pv = PackageVersion(self)
            pv.version = release['tag_name'] or release['name']
            pv.changes = release['body']
            #pv.for_game_build = CURRENT_GAME_BUILD
            pv.date = datetime.datetime.strptime(release['published_at'], "%Y-%m-%dT%H:%M:%SZ")
            pv.date = pv.date.replace(tzinfo=datetime.timezone.utc)
            pv.files = {}
            for asset in release['assets']:
                if asset['name'] in self.filenames:
                    subdir = self.getSubDir()
                    pf = pv.addFile(asset['browser_download_url'], str(subdir / asset['name']))
                    pf.mimetype = asset['content_type']
                    pf.size = asset['size']
            if len(pv.files):
                if latestPV is None or pv.date > latestPublish:
                    latestPV = pv
                    latestPublish = pv.date
        return latestPV
