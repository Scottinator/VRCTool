import hashlib

import collections
import datetime
import json
import os
import string
import tqdm
import requests
import random
from enum import Enum, IntEnum, IntFlag
from pathlib import Path, PurePosixPath
from typing import Any, Dict, List, Optional, Set, Type, Union

import vrctool.http as http
from vrctool.consts import CURRENT_GAME_BUILD, MIN_ACCEPTABLE_GAME_BUILD
from vrctool.decoders import ALL_DECODERS
from vrctool.decoders._base import BaseDecoder
from vrctool.paths import Paths
from vrctool.ratelimit import (BaseRateLimiter, RateLimiterManager,
                               RateLimiterType)
from vrctool.utils import checksum, getFilenameFromURL, sizeof_fmt
from urllib.parse import urlencode, urlparse

from vrctool.logging import PostMessagingBox, getLogger #isort: skip
log = getLogger(__name__)

class EPackageType(IntEnum):
    MOD = 0
    PLUGIN = 1
    OTHER = 2

INSTALLSFILE = Path('.installs')

class EComparisonOperation(Enum):
    EQUALS = '=='
    NOT_EQUALS = '!='
    LESS_THAN = '<'
    LESS_THAN_OR_EQUALS = '<='
    GREATER_THAN = '>'
    GREATER_THAN_OR_EQUALS = '>='

class VersionSpec:
    def __init__(self, data: Optional[str] = None) -> None:
        self.operation: EComparisonOperation = EComparisonOperation.EQUALS
        self.version: str = ''

class PackageInstallation:
    def __init__(self, modid: Optional[str]=None) -> None:
        self.modid: str = modid or ''

        self.packaged_files: Set[Path] = set()
        self.configuration_files: Set[Path] = set()
        self.temporary_files: Set[Path] = set()
        self.log_files: Set[Path] = set()
        self.extracted_files: Set[Path] = set()

    def uninstall(self, include_config: bool=False, include_logs: bool=False) -> None:
        removal_plan = list(self.packaged_files)
        removal_plan += list(self.temporary_files)
        removal_plan += list(self.extracted_files)
        if include_config:
            removal_plan += list(self.configuration_files)
        if include_logs:
            removal_plan += list(self.log_files)

        for filename in removal_plan:
            file = Path(filename)
            if file.is_file():
                log.info('rm %s', file)
                file.unlink()

    def serialize(self) -> dict:
        cwd = Paths.VRC_INSTALL_DIR
        return {
            'm': self.modid,
            'p': [str(x.relative_to(cwd).as_posix()) for x in self.packaged_files],
            'c': [str(x.relative_to(cwd).as_posix()) for x in self.configuration_files],
            't': [str(x.relative_to(cwd).as_posix()) for x in self.temporary_files],
            'l': [str(x.relative_to(cwd).as_posix()) for x in self.log_files],
            'x': [str(x.relative_to(cwd).as_posix()) for x in self.extracted_files],
        }

    def deserialize(self, data: dict) -> dict:
        self.modid = data['m']
        cwd = Paths.VRC_INSTALL_DIR
        self.packaged_files = set([(cwd / x) for x in data['p']])
        self.configuration_files = set([(cwd / x) for x in data['c']])
        self.temporary_files = set([(cwd / x) for x in data['t']])
        self.log_files = set([(cwd / x) for x in data['l']])
        self.extracted_files = set([(cwd / x) for x in data['x']])

    _ALL = None

    @classmethod
    def GetAll(cls) -> Dict[str, 'PackageInstallation']:
        if cls._ALL is None:
            cls._ALL = {}
            if INSTALLSFILE.is_file():
                try:
                    with INSTALLSFILE.open('r') as f:
                        data = json.load(f)
                        if data['v'] == 1:
                            for k,v in data['i'].items():
                                cls._ALL[k] = cls()
                                cls._ALL[k].deserialize(v)
                except Exception as e:
                    log.error('Failed to parse %s, skipping load:', INSTALLSFILE)
                    log.exception(e)
        return cls._ALL

    @classmethod
    def Add(cls, install: 'PackageInstallation') -> None:
        installations = cls.GetAll()
        installations[install.modid] = install
        cls.Save(installations)

    @classmethod
    def Save(cls, installations: Optional[Dict[str, 'PackageInstallation']] = None) -> None:
        if installations is None:
            installations = cls.GetAll()
        with INSTALLSFILE.open('w') as f:
            data = {
                'v':1,
                'i': {k: v.serialize() for k, v in installations.items()}
            }
            json.dump(data, f)
        cls._ALL = installations

    @classmethod
    def Get(cls, modid: str) -> Optional['PackageInstallation']:
        return cls.GetAll().get(modid, None)

class EPackageFlags(IntFlag):
    NONE = 0

    # May contain detectable functions (e.g. weird networked stuff)
    RISKY     = 1
    # Marked as potentially malicious by the moderation staff
    MALICIOUS = 2
    # Marked as in need of review
    SUSPECT   = 4
    # Used in case a repo gets taken down, such as the 3/30 legal threat clusterfuck, or if a mod needs to be temporarily taken down.
    DISABLED  = 8
    # Reviewed by staff
    REVIEWED  = 16
    # Works in all Unity games.
    UNIVERSAL = 32
    # No auto-updating
    NO_AUTO_UPDATE = 64

    #MAX = 64
    #ALL = 127

class EReviewResult(IntEnum):
    NONE = 0
    PASSED = 1
    FAILED = 2

class Review:
    def __init__(self) -> None:
        self.date: datetime.datetime = None
        self.reviewer: str = ''
        self.result: EReviewResult = EReviewResult.NONE
        self.notes: Optional[str] = ''

    def getDateStr(self) -> str:
        #return self.date.strftime('%B %-d, %Y  %-I:%M:%S %p')
        return self.date.isoformat()

    def serialize(self) -> dict:
        return {
            'date': self.date.timestamp(),
            'reviewer': self.reviewer,
            'result': self.result.name,
            'notes': self.notes,
        }

    def deserialize(self, data: dict) -> None:
        self.date = datetime.datetime.fromtimestamp(data['date'], tz=datetime.timezone.utc)
        self.reviewer = data['reviewer']
        self.result = EReviewResult[data['result']]
        self.notes = data.get('notes')

class PackageFile:
    def __init__(self, version: 'PackageVersion') -> None:
        self.version: 'PackageVersion' = version
        self.uri: str = ''
        self.destination: str = ''
        self.size: int = 0
        self.hash: Optional[str] = None
        self.algorithm: Optional[str] = None
        self.mimetype: Optional[str] = None
        self.method: str = 'GET'
        self.params: dict = {}
        self.data: dict = {}
        self.headers: dict = {}
        self.decoders: List[BaseDecoder] = []
        self.fetchDuringInstall: bool = True

    def serialize(self) -> dict:
        o = {
            'uri': self.uri,
            'destination': Path(self.destination).as_posix()
        }
        if self.size > 0:
            o['size'] = self.size
        if self.algorithm and self.hash:
            o['hash'] = self.algorithm+'$'+self.hash
        if self.mimetype:
            o['mime-type'] = self.mimetype
        if self.method != 'GET':
            o['method'] = self.method
        if len(self.params) > 0:
            o['params'] = self.params
        if len(self.data) > 0:
            o['data'] = self.data
        if len(self.headers) > 0:
            o['headers'] = self.headers
        if len(self.decoders) > 0:
            dcl: List[dict] = []
            for v in self.decoders:
                dcl.append(v.serialize())
            o['decoders'] = dcl
        if not self.fetchDuringInstall:
            o['fetch-during-install'] = self.fetchDuringInstall
        return o

    def deserialize(self, data: dict) -> None:
        self.uri = data['uri']
        self.destination = data.get('destination', 'Mods')
        self.size = data.get('size', 0)
        self.algorithm = None
        self.hash = None
        if 'hash' in data:
            self.algorithm, self.hash = data['hash'].split('$')
        self.method  = data.get('method', 'GET').upper()
        self.params  = data.get('params', {})
        self.data    = data.get('data', {})
        self.headers = data.get('headers', {})
        self.decoders = []
        dcl: Union(Dict[str, dict], List[dict]) = data.get('decoders', [])
        if isinstance(dcl, list):
            for v in dcl:
                t: str = v['type']
                d = ALL_DECODERS[t](self)
                d.deserialize(v)
                self.decoders.append(d)
        elif isinstance(dcl, dict):
            for k,v in dcl.items():
                d = ALL_DECODERS[k](self)
                d.deserialize(v)
                self.decoders.append(d)
        self.mimetype = data.get('mimetype', None)
        self.fetchDuringInstall = data.get('fetch-during-install', True)

    def fetchTo(self, basedir: Path) -> None:
        localpath = basedir / self.destination
        if len(self.decoders) > 0:
            localpath = localpath.parent / (localpath.name + '.encoded')
        if not localpath.parent.is_dir():
            localpath.parent.mkdir(parents=True)

        headers = dict(self.headers)
        http.fixRequestHeaders(headers, self.uri, str(localpath))
        rl = self.version.package.getRateLimiter(self.uri)
        if rl:
            rl.delayUntilUsable()
        hitCache=False
        try:
            with requests.request(self.method, self.uri, params=self.params, headers=self.headers, data=self.data, stream=True) as res:
                if res.status_code == 304:
                    log.info('Got HTTP 304 - Not Modified, skipping.')
                    hitCache=True
                else:   
                    res.raise_for_status()
                    total_size_in_bytes= int(res.headers.get('content-length', 0))
                    log.info(f'Fetching {self.uri} -> {self.destination}')
                    with tqdm.tqdm(total=total_size_in_bytes, unit='iB', unit_scale=True) as progress:
                        with localpath.open('wb') as f:
                            for chunk in res.iter_content(1024):
                                progress.update(len(chunk))
                                f.write(chunk)
                    http.updateMetadata(self.uri, self.destination, res)
        finally:
            # Always decode.
            if localpath.is_file():
                self.decode(basedir)
            if not hitCache:
                self.version.package.pushRateLimitedEvent(self.uri)

    def calculateFrom(self, basedir: Path) -> None:
        localpath = basedir / self.destination
        self.size = localpath.stat().st_size
        self.algorithm = 'SHA512'
        self.hash = checksum(hashlib.sha512, localpath)
        #self.mimetype = magic.from_file(localpath, mime=True)

    def checkIntegrityAt(self, basedir: Path) -> bool:
        localpath = basedir / self.destination
        success = True
        if not localpath.is_file():
            log.warning('File does not exist')
            return False
        if self.size != localpath.stat().st_size:
            log.warning('Size: %d B (expected) != %d B (actual)', self.size, localpath.stat().st_size)
            success=False
        else:
            log.info('Size: %s (%d B) - OK', sizeof_fmt(self.size), self.size)
        if self.algorithm == 'SHA512':
            actual = checksum(hashlib.sha512, localpath)
            if self.hash != actual:
                log.warning('SHA512: %s (expected) != %s (actual)', self.hash, actual)
                success = False
            else:
                log.info('SHA512: %s - OK', self.hash)
        elif self.algorithm is None:
            log.warning('No checksum specified for this file, skipping. (SECURITY RISK)')
        else:
            log.error('Checksum algorithm %r is not supported.', self.algorithm)
        #self.mimetype = magic.from_file(localpath, mime=True)
        return success


    def decode(self, basedir: Path) -> None:
        #print(repr(self.decoders))
        if len(self.decoders) == 0:
            return
        newfile = basedir / self.destination
        oldfile = newfile.parent / (newfile.name + '.encoded')
        chain = [oldfile]
        cleanup = [oldfile]
        alpha = string.ascii_lowercase + string.ascii_uppercase + string.digits
        end = len(self.decoders) - 1
        try:
            i = 0
            for decoder in self.decoders:
                fn: Path
                if i<end:
                    fn = newfile.parent / ''.join([random.choice(alpha) for _ in range(10)])+'.dat'
                    cleanup += [fn]
                else:
                    fn = newfile
                with log.info('Decoding %s with decoder [%s] to %s...', chain[-1], decoder.TYPEID, fn):
                    decoder.decode(basedir, chain[-1], fn)
                chain += [fn]
        finally:
            for trash in cleanup:
                if trash.is_file():
                    log.info('Cleaning up %s...', trash)
                    trash.unlink()

class EVersionFlags(IntFlag):
    NONE = 0
    BROKEN = 1

class PackageVersion:
    def __init__(self, package, v: Optional[dict] = None) -> None:
        self.package = package
        self.date: datetime.datetime = None
        self.version: str = ''
        self.changes: str = ''
        self.for_game_build: int = CURRENT_GAME_BUILD
        self.review: Optional[Review] = None
        self.files: Dict[str, PackageFile] = {}
        self.flags: EVersionFlags = EVersionFlags.NONE

        if v is not None:
            self.deserialize(v)

    def hasFlag(self, flag: Union[EVersionFlags, int]) -> bool:
        return (self.flags & flag) == flag

    def renameFile(self, oldPath: Path, newPath: Path) -> None:
        o = str(oldPath.as_posix())
        n = str(newPath.as_posix())
        f = self.files[o]
        self.files[n] = f
        del self.files[o]
        f.destination = newPath

    def getDateStr(self) -> str:
        #return self.date.strftime('%B %-d, %Y  %-I:%M:%S %p')
        return self.date.isoformat()

    def addFile(self, uri, dest) -> PackageFile:
        pf = PackageFile(self)
        pf.uri = uri
        pf.destination = dest
        self.files[pf.destination] = pf
        return pf

    def canInstall(self, game_build: int) -> bool:
        if (self.flags & EVersionFlags.BROKEN) == EVersionFlags.BROKEN:
            return False
        if self.for_game_build > game_build:
            return False
        return True

    def serialize(self) -> dict:
        o = {
            'date': self.date.timestamp(),
            'version': self.version,
            'changes': self.changes,
            'for-game-build': self.for_game_build,
            'files': {k: pf.serialize() for k, pf in self.files.items()},
            'review': self.review.serialize() if self.review else None,
        }
        if self.flags != EVersionFlags.NONE:
            o['flags'] = []
            for m in EVersionFlags.__members__.values():
                if m != EVersionFlags.NONE:
                    o['flags'].append(m.name)
        return o

    def deserialize(self, data: dict) -> None:
        self.date = datetime.datetime.fromtimestamp(data['date'], tz=datetime.timezone.utc)

        self.version = data['version']
        self.changes = data.get('changes', '')
        if 'url' in data and 'hash' in data:
            pf = PackageFile(self)
            pf.uri = data['url']
            pf.destination = str(self.getSubDir() / getFilenameFromURL(data['url']))
            pf.algorithm, pf.hash = data['hash'].split('$')
            self.files[pf.destination] = pf
        if 'files' in data:
            for _, pfdata in data['files'].items():
                pf = PackageFile(self)
                pf.deserialize(pfdata)
                self.files[pf.destination] = pf
        self.for_game_build = data['for-game-build']
        if 'review' in data:
            self.review = Review()
            self.review.deserialize(data['review'])
        if 'flags' in data:
            self.flags = EVersionFlags.NONE
            if isinstance(data['flags'], int):
                self.flags = EVersionFlags(data['flags'])
            if isinstance(data['flags'], list):
                for f in data['flags']:
                    self.flags |= EVersionFlags[f.upper()]
            
class Package:
    TYPEID: str = ''

    KEEP = set()

    PACKAGE_SOURCES = {}

    @classmethod
    def Load(cls, data: dict, from_: Optional[str] = None, skip_installation: bool = False) -> 'Package':
        frm = from_ or data['from']
        pkg: Package = cls.PACKAGE_SOURCES[frm](data['id'])
        pkg.deserialize(data, skip_installation=skip_installation)
        return pkg

    @classmethod
    def LoadFromIndex(cls, data: dict) -> 'Package':
        typ, data = next(iter(data.items()))
        data['type'] = typ
        return cls.Load(data, data['from'], True)

    def __init__(self, _id: str, typ: EPackageType) -> None:
        self.type = typ
        self.install_path: Path = None
        self.id: str = _id
        self.aliases: Set[str] = set()
        self.name: str = ''
        self.description: str = ''
        self.authors: List[str] = []
        self.source_url: Optional[str] = None
        self.versions: Dict[str, PackageVersion] = {}
        self.flags: EPackageFlags = EPackageFlags(0)
        self.installation = PackageInstallation(_id)
        self.toml_path: Path = None
        self.tags: Set[str] = set()

        self.requires: Set[str] = set()
        self.conflicts: Set[str] = set()

        self.config_files:    Set[Path] = set()
        self.log_files:       Set[Path] = set()
        self.extracted_files: Set[Path] = set()

        self.warnedOf: EPackageFlags = EPackageFlags.NONE

        self._last_request_hit_cache = False

    def copyFromOld(self, other: 'Package') -> None:
        self.type = getattr(other, 'type', self.type)
        self.install_path = getattr(other, 'install_path', self.install_path)
        self.id = getattr(other, 'id', self.id)
        self.aliases = getattr(other, 'aliases', self.aliases)
        self.name = getattr(other, 'name', self.name)
        self.description = getattr(other, 'description', self.description)
        self.authors = getattr(other, 'authors', self.authors)
        self.source_url = getattr(other, 'source_url', self.source_url)
        self.versions = getattr(other, 'versions', self.versions)
        self.flags = getattr(other, 'flags', self.flags)
        self.installation = getattr(other, 'installation', self.installation)
        self.toml_path = getattr(other, 'toml_path', self.toml_path)
        self.tags = getattr(other, 'tags', self.tags)

        self.requires = getattr(other, 'requires', self.requires)
        self.conflicts = getattr(other, 'conflicts', self.conflicts)

        self.config_files = getattr(other, 'config_files', self.config_files)
        self.log_files = getattr(other, 'log_files', self.log_files)
        self.extracted_files = getattr(other, 'extracted_files', self.extracted_files)


    def hasFlag(self, flag: EPackageFlags) -> bool:
        return (self.flags & flag) == flag

    def userHasBeenWarnedOf(self, flag: EPackageFlags) -> bool:
        return (self.warnedOf & flag) == flag

    def install(self, msgbox: PostMessagingBox, pv: PackageVersion, cwd: Path) -> None:
        for filerecord in pv.files.values():
            with log.info('%s:', filerecord.destination):
                if not filerecord.fetchDuringInstall:
                    log.info('Skipped (not to be installed)')
                    continue

                if not filerecord.checkIntegrityAt(cwd):
                    filerecord.fetchTo(cwd)
                    assert filerecord.checkIntegrityAt(cwd), f'File {cwd / filerecord.destination} failed integrity check'
                else:
                    log.info('Integrity check OK, not updating.')
                self.installation.packaged_files.add(cwd / filerecord.destination)
                self.KEEP.add(cwd / filerecord.destination)
        for p in self.extracted_files:
            self.KEEP.add(p)
        for p in self.installation.extracted_files:
            self.KEEP.add(p)

    def getLatestAvailableVersion(self, game_build: int) -> Optional[PackageVersion]:
        for v in sorted(self.versions.values(), key=lambda x: x.date, reverse=True):
            #print(f'{v.version}\t{v.date}')
            if not v.canInstall(game_build):
                log.warning('Package %r version %r has problems, skipping.', self.id, v.version)
                continue
            return v
        return None

    def _do_install(self, msgbox: PostMessagingBox, game_build: int) -> None:
        pv: PackageVersion = self.getLatestAvailableVersion(game_build)
        if pv is None:
            with log.warning('Failed to find any package versions, auto-discovering.'):
                pv = self.fetchLatestRelease()
        if (self.flags & EPackageFlags.UNIVERSAL) == EPackageFlags(0) and pv.for_game_build < game_build:
            msgbox.addWarning(f'Package version {pv.version} was built for VRC build {pv.for_game_build}, whilst your VRC install is at build {game_build}. You may experience bugs or crashes.')
        if pv is None:
            raise UnableToFindReleaseError
        log.info('Selected version %r', pv.version)

        oldinstallation = PackageInstallation.Get(self.id)

        # Remove old files.
        if oldinstallation is not None:
            for pfilename in oldinstallation.packaged_files:
                if pfilename not in self.installation.packaged_files:
                    log.info('- %s', pfilename)
                    if os.path.isfile(pfilename):
                        os.remove(pfilename)

        #self.install()
        cwd = Paths.VRC_INSTALL_DIR
        if pv:
            self.install(msgbox, pv, cwd)

        [self.installation.configuration_files.add(cwd / x) for x in self.config_files]
        [self.installation.log_files.add(cwd / x) for x in self.log_files]
        [self.installation.packaged_files.add(cwd / x) for x in self.extracted_files]

        if len(self.extracted_files) > 0:
            with log.info('Extracted Files:'):
                for xfile in self.extracted_files:
                    log.info(' * %s', cwd / xfile)
        if len(self.config_files) > 0:
            with log.info('Configuration Files:'):
                for cfgfile in self.config_files:
                    log.info(' * %s', cwd / cfgfile)
        if len(self.log_files) > 0:
            with log.info('Log Files:'):
                for logfile in self.log_files:
                    log.info(' * %s', cwd / logfile)

        # Announce new files.
        if oldinstallation is not None:
            for pfilename in self.installation.packaged_files:
                if pfilename not in oldinstallation.packaged_files:
                    log.info('+ %s', pfilename)

        PackageInstallation.Add(self.installation)

    def getInstallLocation(self, basename: str) -> None:
        if self.install_path is not None:
            return self.install_path
        if self.type == EPackageType.MOD:
            return Paths.MOD_DIR / basename
        elif self.type == EPackageType.PLUGIN:
            return Paths.PLUGIN_DIR / basename
        elif self.type == EPackageType.OTHER:
            return self.install_path

    def getRateLimiter(self, uri: str) -> Optional[BaseRateLimiter]:
        return None

    def isRateLimited(self, uri: str) -> bool:
        return False

    def pushRateLimitedEvent(self, uri: str) -> None:
        return

    def serialize(self) -> dict:
        #print(dir(self.type))
        o = {
            'id': self.id.lower(),
            'name': self.name,
            'type': self.type.name.lower(),
            'from': self.TYPEID,
        }
        if len(self.aliases) > 0:
            o['aliases'] = list(self.aliases)
        if len(self.tags) > 0:
            o['tags'] = sorted(list(self.tags))
        if self.description is not None:
            o['description'] = self.description
        if len(self.requires):
            o['requires'] = sorted(list(self.requires))
        if len(self.conflicts):
            o['conflicts'] = sorted(list(self.conflicts))
        if self.toml_path is not None:
            o['path'] = self.toml_path.as_posix()

        if self.flags != EPackageFlags.NONE:
            fstr = []
            for fv in EPackageFlags:
                if fv == EPackageFlags.NONE:
                    continue
                if (self.flags & fv) == fv:
                    fstr.append(fv.name)
            o['flags'] = fstr
        if len(self.config_files) > 0 or len(self.log_files) > 0 or len(self.extracted_files) > 0:
            o['files'] = {}
            if len(self.config_files):
                o['files']['config'] = [Path(x).as_posix() for x in self.config_files]
            if len(self.log_files):
                o['files']['logs'] = [Path(x).as_posix() for x in self.log_files]
            if len(self.extracted_files):
                o['files']['extracted'] = [Path(x).as_posix() for x in self.extracted_files]
        if len(self.versions) > 0:
            o['versions'] = {v.version: v.serialize() for _, v in self.versions.items()}
        return o

    def sortVersionsBy(self, x):
        #print(repr(x))
        return x[1]['date']

    def deserialize(self, data: dict, skip_installation: bool = False) -> None:
        self.id = data.get('id', self.id).lower()
        self.aliases = set([x.lower() for x in data.get('aliases', [])])
        
        self.name = data.get('name', self.id)
        self.type = EPackageType[data.get('type', 'MOD').upper()]
        self.toml_path = Path(data.get('path', None)) if 'path' in data else None
        self.tags = set(data.get('tags', []))
        self.description = data.get('description')
        self.requires = set(data.get('requires', []))
        self.conflicts = set(data.get('conflicts', []))
        self.flags = EPackageFlags(0)
        flags = data.get('flags', None)
        if flags is not None:
            if isinstance(flags, list):
                for fn in flags:
                    self.flags |= EPackageFlags[fn]
            elif isinstance(flags, int):
                self.flags = EPackageFlags(flags)
            else:
                raise Exception(f'flags is of invalid type {type(flags)}, expected int or List[str]')
        if 'files' in data:
            self.config_files = data['files'].get('config', [])
            self.log_files = data['files'].get('logs', [])
            self.extracted_files = data['files'].get('extracted', [])
        if 'versions' in data:
            self.versions = collections.OrderedDict()
            for _, v in sorted(data['versions'].items(), key=self.sortVersionsBy, reverse=True):
                self.versions[v['version']] = PackageVersion(self, v)

        if not skip_installation:
            self.installation = PackageInstallation.Get(self.id)
            if self.installation is None:
                self.installation = PackageInstallation(self.id)

    def serializeToConfig(self) -> dict:
        o = {'id': self.id}
        if self.warnedOf != EPackageFlags.NONE:
            o['warned'] = self.warnedOf.value
        return o

    def isSatisfied(self, installed: Set[str]) -> bool:
        if len(self.requires) > 0:
            if not all([x in installed for x in self.requires]):
                return False
        if len(self.conflicts) > 0:
            if not any([x in installed for x in self.conflicts]):
                return False
        return True

    def canInstall(self) -> bool:
        return self.hasFlag(EPackageFlags.REVIEWED) and not self.hasFlag(EPackageFlags.DISABLED) and not self.hasFlag(EPackageFlags.MALICIOUS)

    def mustUninstall(self) -> bool:
        return self.hasFlag(EPackageFlags.MALICIOUS)

    def getWarnings(self) -> Dict[EPackageFlags, str]:
        o = {}
        if self.hasFlag(EPackageFlags.MALICIOUS):
            o[EPackageFlags.MALICIOUS] = '!THIS PACKAGE INCLUDES MALICIOUS CONTENT AND WILL BE UNINSTALLED.'
        if self.hasFlag(EPackageFlags.RISKY):
            o[EPackageFlags.RISKY] = 'This package includes behaviour that is risky to use.'
        if self.hasFlag(EPackageFlags.DISABLED):
            o[EPackageFlags.DISABLED] = 'This package has been temporarily disabled by the repository owner.'
        if self.hasFlag(EPackageFlags.SUSPECT):
            o[EPackageFlags.SUSPECT] = 'This package needs to be further analyzed.'
        if not self.hasFlag(EPackageFlags.REVIEWED):
            o[EPackageFlags.REVIEWED] = 'This package needs to be reviewed.'
        return o

    def fetchLatestRelease(self, cache=False) -> Optional[PackageVersion]:
        return None

    def getLatestVersion(self) -> Optional[PackageVersion]:
        return next(iter(sorted(self.versions.values(), key=lambda x: x.date, reverse=True)))

    def fetchDownloadURLs(self) -> Optional[List[str]]:
        pv = self.fetchLatestRelease()
        if pv:
            log.info('Found %d URIs in release %r', len(pv.files), pv.version)
            return [f.uri for f in pv.files.values()]
        return None

    def getSubDir(self) -> Path:
        if self.type == EPackageType.MOD:
            return PurePosixPath('Mods')
        elif self.type == EPackageType.PLUGIN:
            return PurePosixPath('Plugins')
        else:
            return PurePosixPath('UNKNOWN')



class UnableToFindReleaseError(Exception):
    pass


def packagetype(cls: Type[Package]) -> Type[Package]:
    Package.PACKAGE_SOURCES[cls.TYPEID] = cls
    return cls
