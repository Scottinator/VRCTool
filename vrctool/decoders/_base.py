from pathlib import Path

class BaseDecoder:
    TYPEID: str = ''
    def __init__(self, file: 'PackageFile') -> None:
        self.file: 'PackageFile' = file

    def addExtractedFile(self, path: Path) -> None:
        self.file.version.package.installation.extracted_files.add(path)
        self.file.version.package.KEEP.add(path)
        
    def addExtractedDir(self, dirpath: Path) -> None:
        for path in dirpath.iterdir():
            if path.is_file():
                self.addExtractedFile(path)
            if path.is_dir():
                self.addExtractedDir(path)

    def serialize(self) -> dict:
        return {'type': self.TYPEID}

    def deserialize(self, data: dict) -> None:
        return

    def decode(self, basedir: Path, inputpath: Path, outputpath: Path) -> None:
        pass
