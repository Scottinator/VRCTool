from .base64 import Base64Decoder
from .zip import ZipDecoder

ALL_DECODERS = {
    'base64': Base64Decoder,
    'zip': ZipDecoder
}
