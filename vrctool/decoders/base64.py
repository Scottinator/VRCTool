import binascii
from pathlib import Path

from ._base import BaseDecoder

from vrctool.logging import getLogger #isort: skip
log = getLogger(__name__)

class Base64Decoder(BaseDecoder):
    TYPEID = 'base64'
    def decode(self, basedir: Path, inputpath: Path, outputpath: Path) -> None:
        outputpath.write_bytes(binascii.a2b_base64(inputpath.read_text()))
