import os
import zipfile
from pathlib import Path
from typing import Dict, Optional

import tqdm

from ._base import BaseDecoder

from vrctool.logging import getLogger #isort: skip
log = getLogger(__name__)


class ZipDecoder(BaseDecoder):
    TYPEID = 'zip'
    def __init__(self, file) -> None:
        super().__init__(file)
        self.extract: Dict[str, str] = {}
        self.password: Optional[str] = None

    def serialize(self) -> dict:
        data = super().serialize()
        data['extract'] = self.extract
        if self.password:
            data['password'] = self.password
        return data

    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
        self.extract = data['extract']
        self.password = data.get('password')

    def _extractMember(self, z: zipfile.ZipFile, member: str, path: Path) -> None:
        from vrctool.packages import Package    
        log.info(f'Extracting {member} to {path}...')
        z.extract(member, path.parent, pwd=self.password)
        
        self.addExtractedFile(path)

    def decode(self, basedir: Path, inputpath: Path, outputpath: Path) -> None:
        log.info('ZIP: %s: %s -> %s', basedir, inputpath, outputpath)
        with zipfile.ZipFile(inputpath, mode='r') as z:
            for src, dest in self.extract.items():
                if src == '*':
                    for member in tqdm.tqdm(z.namelist(), unit='file'):
                        self._extractMember(z, member, basedir / dest)
                else:
                    self._extractMember(z, src, basedir / dest)
        os.replace(inputpath, outputpath)
        if inputpath.is_file():
            inputpath.unlink()
