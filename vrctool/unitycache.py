'''
This is a python interface for Unity3D's Caching/Cache/CachedAssetBundle.

This was all gathered from black-box analysis and guesswork.

The cache hash, for instance, was figured out by trying every possible hash and
CRC algorithm I could find against known samples from my own avatars.
'''
import datetime
import hashlib
import os
import shutil
import time
from pathlib import Path
from typing import Dict, List

from vrctool.utils import sizeof_fmt

from vrctool.logging import getLogger #isort: skip
log = getLogger(__name__)

def _parse_metafile(path: Path) -> List[str]:
    with open(path, 'r') as f:
        return [x.strip() for x in f.readlines()]

class CachedBundleVersion:
    def __init__(self, bundle: 'CachedAssetBundle') -> None:
        self.bundle: 'CachedAssetBundle' = bundle
        self.hash: bytes = b''
        self.unknown_info_1: int = -1
        self.modified: datetime.datetime = None
        self.unknown_info_2: int = 1

        self.info_path: Path
        self.data_path: Path
        self.size: int = 0

    @property
    def version(self) -> int:
        return int.from_bytes(self.hash[12:16], byteorder='little')

    @version.setter
    def set_version(self, value: int) -> None:
        self.hash = b''.join([x.to_bytes(4, byteorder='little') for x in [0, 0, 0, value]])

    def loadFrom(self, path: Path) -> None:
        self.info_path = path
        self.deserialize_info(_parse_metafile(path))

        self.size = os.path.getsize(self.data_path)

    def deserialize_info(self, data: List[str]) -> None:
        '''
        -1
        1619316349
        1
        __data
        '''
        self.unknown_info_1 = int(data[0])
        assert self.unknown_info_1 == -1
        self.modified = datetime.datetime.fromtimestamp(int(data[1]), tz=datetime.timezone.utc)
        self.unknown_info_2 = int(data[2])
        assert self.unknown_info_2 == 1
        self.data_path = self.info_path.parent / data[3]

    def remove(self) -> None:
        if self.info_path.parent.is_dir():
            log.info('rm %s', self.info_path.parent)
            #shutil.rmtree(self.info_path.parent)
        del self.bundle.versions[self.hash]


class CachedAssetBundle:
    def __init__(self, cache: 'Cache') -> None:
        self.cache: 'Cache' = cache
        self.name: str = ''
        self.versions: Dict[bytes, CachedBundleVersion] = {}

        self.path: Path = None

    def remove(self) -> None:
        if self.path.is_dir():
            log.info('rm %s', self.path)
            shutil.rmtree(self.path)
        del self.cache.entries[self.name]

    def getVersionByInt(self, version: int) -> 'CachedBundleVersion':
        # Second dir is 4 ints.  First 3 are 0, last is version.
        hash = b''.join([x.to_bytes(4, byteorder='little') for x in [0, 0, 0, version]])
        return self.versions[hash]

class Cache:
    def __init__(self) -> None:
        self.id = 0
        self.modified: datetime.datetime = None
        self.expires: datetime.datetime = None
        self.expireIn: datetime.timedelta = datetime.timedelta(days=150)
        self.maxSize: int = 21474836480 # Unity Default: 4294967296

        self.entries = {}
        self.invalidDirs = []

    def loadFromDir(self, basedir: Path) -> None:
        METADATA = basedir / '__info'
        if METADATA.is_file():
            self.deserialize_info(_parse_metafile(METADATA))

        # cache_dir / package / hash OR [0, 0, 0, version](LE)
        ce: CachedAssetBundle
        for bundledir in basedir.iterdir():
            if bundledir.is_file():
                continue
            ce = CachedAssetBundle(self)
            ce.name = bundledir.name
            ce.path = bundledir
            for bundlehashdir in bundledir.iterdir():
                if bundlehashdir.is_file():
                    continue
                infofile = bundlehashdir / '__info'
                if not infofile.is_file():
                    self.invalidDirs += [bundlehashdir]
                    log.warning('Cache entry [%r/%r] is missing an __info file.', ce.name, bundlehashdir.name)
                    continue
                bv = CachedBundleVersion(ce)
                bv.hash = bytes.fromhex(bundlehashdir.name)
                bv.loadFrom(infofile)
                ce.versions[bv.hash] = bv
            self.entries[ce.name] = ce

    def deserialize_info(self, data: List[str]) -> None:
        '''
        1622007432
        1
        1618973948
        '''
        self.expires = datetime.datetime.fromtimestamp(int(data[0]), tz=datetime.timezone.utc)
        self.id = int(data[1])
        self.modified = datetime.datetime.fromtimestamp(int(data[2]), tz=datetime.timezone.utc)

    def getByName(self, name) -> CachedAssetBundle:
        return self.entries[name]

    def getByNameAndHash(self, name: str, hash: bytes) -> CachedBundleVersion:
        return self.entries[name].versions[hash]

    def getByNameAndVersion(self, name: str, version: int) -> CachedBundleVersion:
        return self.getByName(name).getVersionByInt(version)

    # VRC-specific
    def getByFileID(self, fileID: str) -> CachedAssetBundle:
        # First dir is first 8 bytes of the SHA-256 of the asset ID.
        truncated_hash = hashlib.sha256(fileID.encode('utf-8')).hexdigest().upper()[:16]
        return self.entries[truncated_hash]

    # VRC-specific
    def getByFileIDAndVersion(self, fileID: str, version: int) -> CachedBundleVersion:
        return self.getByFileID(fileID).getVersionByInt(version)

    def getAll(self) -> List[CachedBundleVersion]:
        for entry in self.entries.values():
            for version in entry.versions.values():
                yield version

    def clean(self) -> None:
        allVersions=[]
        for entry in list(self.entries.values()):
            if len(entry.versions) == 0:
                entry.remove()
                continue
            for version in list(entry.versions.values()):
                if version.modified + self.expireIn < datetime.datetime.now(tz=datetime.timezone.utc):
                    log.info('%s/%s expired', entry.name, version.hash)
                    version.remove()
                    continue
                    allVersions += [(entry, version)]
            if len(entry.versions) == 0:
                entry.remove()

        allVersions.sort(key=lambda entry, version: version.modified, reverse=True)
        def getTotalSize():
            tsz = 0
            for entry in list(self.entries.values()):
                if len(entry.versions) == 0:
                    entry.remove()
                    continue
                for version in list(entry.versions.values()):
                    tsz += version.size
            return tsz
        while (tsz := getTotalSize()) > self.maxSize:
            log.info('Cache is %s over the %s limit.', sizeof_fmt(tsz-self.maxSize), self.maxSize)
            entry, version = allVersions.pop()
            with log.info('Removing %s/%s...', entry.name, version.hash):
                version.remove()
