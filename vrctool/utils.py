import os
import struct
from typing import BinaryIO, List, Literal
from urllib.parse import urlparse

from vrctool.logging import getLogger #isort: skip
log = getLogger(__name__)

# https://stackoverflow.com/questions/1094841/get-human-readable-version-of-file-size
def sizeof_fmt(num, suffix='B'):
    for unit in ['','Ki','Mi','Gi','Ti','Pi','Ei','Zi']:
        if abs(num) < 1024.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, 'Yi', suffix)

# https://stackoverflow.com/questions/47093561/remove-empty-folders-python
def remove_empty_folders(path_abs):
    walk = list(os.walk(path_abs))
    for path, _, _ in walk[::-1]:
        if len(os.listdir(path)) == 0:
            os.rmdir(path)


def checksum(algo, path: str, byte_digest: bool=False) -> None:
    with open(path, "rb") as f:
        file_hash = algo()
        while chunk := f.read(8192):
            file_hash.update(chunk)
        if byte_digest:
            return file_hash.digest()
        return file_hash.hexdigest()


def fixISOTS(ts: str) -> str:
    if ts.endswith('Z'):
        return ts[:-1]+'+00:00'
    return ts
    
def getFilenameFromURL(uri: str) -> str:
    return os.path.basename(urlparse(uri).path)

def del_empty_dirs(src_dir: str, quiet=False) -> int:
    '''
    Removes empty directories.

    :param src_dir:
        Root of directory tree to search for empty directories.
    :param quiet:
        Squelches log messages about removing empty directories.
    :returns:
        Count of removed directories.
    '''
    ndeleted = -1
    totalDel = 0
    while ndeleted != 0:
        ndeleted = 0
        # Listing the files
        for dirpath, dirnames, filenames in os.walk(src_dir, topdown=False):
            #print(dirpath, src_dir)
            if dirpath == src_dir:
                continue
            #print(dirpath, len(dirnames), len(filenames))
            if len(filenames) == 0 and len(dirnames) == 0:
                if not quiet:
                    log.info('Removing %s (empty)...', dirpath)
                os.rmdir(dirpath)
                ndeleted += 1
                totalDel += 1
    return totalDel

def getStrings(f: BinaryIO) -> List[str]:
    buf = ''
    cv = 0
    while c := f.read(1):
        cv = c[0]
        if cv >= 0x20 and cv < 0x7F:
            buf += c.decode('ascii')
        elif buf != '':
            yield buf
            buf = ''

# https://github.com/K0lb3/UnityPy/blob/ab269fc21578b193acd741d41748f082ba41cc8b/UnityPy/streams/EndianBinaryReader.py#L94
def read_aligned_string(f: BinaryIO, endianness: Literal['<', '>'] = '<') -> str:
    length = struct.unpack(endianness + "i", f.read(4))[0]
    if length > 0:
        string = f.read(length).decode('ascii')
        alignment = 4
        f.seek((alignment - f.tell() % alignment) % alignment, os.SEEK_CUR)
        return string
