import mmap
import re
from enum import Enum

from .paths import Paths
from .utils import getStrings, read_aligned_string

REG_VRCHAT_VERSION = re.compile(r'(?P<major>\d{4})\.(?P<minor>\d+)\.(?P<revision>\d+)(p(?P<patch>\d+))?\-(?P<build>\d+)\-\-(?P<channel>[A-Za-z]+)')
SIG_VRC_COMMITHASH = b'\x0A\x00\x00\x00commitHash'

class VRChatChannel(Enum):
    RELEASE = 'Release'


class VRChatVersion:
    def __init__(self) -> None:
        self.prefix: str = 'w_' # wtf does this mean
        self.major: int = 0
        self.minor: int = 0
        self.revision: int = 0
        self.patch: int = 0
        self.build: int = 0
        self.channel: VRChatChannel = VRChatChannel.RELEASE
        self.hash: str = ''

    def fromMatch(self, m) -> None:
        self.major = int(m['major'])
        self.minor = int(m['minor'])
        self.revision = int(m['revision'])
        self.patch = int(m['patch']) if m.group('patch') else 0
        self.build = int(m['build'])
        self.channel = VRChatChannel(m['channel'])

    def __str__(self) -> str:
        return f'{self.getAppVersion()} ({self.hash})'

    def getPatchStr(self) -> str:
        return f'p{self.patch}' if self.patch else ''

    def getAppVersion(self) -> str:
        return f'{self.major}.{self.minor}.{self.revision}{self.getPatchStr()}-{self.build}--{self.channel.value}'

    def getPrefixedVersion(self) -> str:
        return f'{self.prefix}{self.major}.{self.minor}.{self.revision}{self.getPatchStr()}-{self.hash}'

    def serialize(self) -> dict:
        return {
            'prefix': self.prefix,
            'major': self.major,
            'minor': self.minor,
            'revision': self.revision,
            'patch': self.patch,
            'build': self.build,
            'channel': self.channel.value,
            'hash': self.hash,
        }

def getVRCVersion() -> VRChatVersion:
    with open(Paths.GLOBAL_GAME_MANAGERS_FILE, 'rb') as f:
        for string in getStrings(f):
            if m := REG_VRCHAT_VERSION.match(string):
                vrcver = VRChatVersion()
                vrcver.fromMatch(m)
                break
    # \x0A\x00\x00\x00commitHash\x00\x00\x0A\x00\x00\x000123456789\x00\x00
    # |     strlen    | str     |padding|    strlen     |   str   | pad
    with open(Paths.RESOURCES_ASSETS_FILE, 'r+b') as f:
        mm = mmap.mmap(f.fileno(), 0)
        pos = mm.find(SIG_VRC_COMMITHASH)
        f.seek(pos)
        assert read_aligned_string(f) == 'commitHash'
        vrcver.hash = read_aligned_string(f)
    return vrcver
