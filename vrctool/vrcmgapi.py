from vrctool import logging
from vrctool.utils import fixISOTS
import requests, json, random, datetime, base64
from pathlib import Path
from vrctool.http import requestIfModified
from enum import Enum, IntEnum

from typing import List, Dict, Optional
from vrctool import http
from vrctool.consts import VERSION

from dateutil.parser import isoparse

from vrctool.logging import getLogger #isort: skip
log = getLogger(__name__)

ENDPOINTS: str = [
    'https://api.vrcmg.com/v0/'
]

class EApprovalStatus(IntEnum):
    NOT_APPROVED = 0
    APPROVED     = 1
    UNKNOWN_2    = 2

class EVRCMGModType(Enum):
    MOD = 'Mod'
    PLUGIN = 'Plugin'
    UNKNOWN = ''

class VRCMGModVersion:
    def __init__(self, mod: 'VRCMGMod' = None) -> None:
        self.mod: 'VRCMGMod' = mod

        '''
        {
        "_version": 8,
        "ApprovalStatus": 1,
        "reason": "im abusing staff powers get fuked",
        "name": "OwO Mod",
        "modversion": "5.0.0-Cutie",
        "vrchatversion": "Universal",
        "loaderversion": "0.2.7.4",
        "modtype": "Mod",
        "author": "DubyaDude (Help from Hector Panzer, Herp Derpinstine and xKiraiChan)",
        "description": "It OwOifies text!\nConfig can be found in ``Mods\\OwO_Mod.txt``\n\nOwOifies:\n - MelonLogger\n - Text\n - TextMesh",
        "downloadlink": "https://api.vrcmg.com/v0/mods/33/OwO-Mod.dll",
        "sourcelink": "https://github.com/DubyaDude/OwO-Mod",
        "discord": "<REDACTED>",
        "hash": "PilqTZLI4sufd39zl/FALW+tJbynnT6G1dBZf5kqxJA=",
        "changelog": null,
        "updatedate": "2021-04-03T02:25:50.171Z"
        }
        '''

        self.status: EApprovalStatus = EApprovalStatus(0)
        self.reason: str = ''
        self.name: str = ''
        self.modVersion: str = ''
        self.vrchatVersion: str = ''
        self.loaderVersion: str = ''
        self.modType: EVRCMGModType = EVRCMGModType.UNKNOWN
        self.author: str = ''
        self.description: str = ''
        self.downloadLink: Optional[str] = ''
        self.sourceLink: Optional[str] = ''
        self.discord: Optional[str] = ''
        self.hash: Optional[bytes] = ''
        self.changelog: Optional[str] = None
        self.updateDate: Optional[datetime.datetime] = None

    def deserialize(self, data: dict) -> None:
        self.status = EApprovalStatus(data.get('ApprovalStatus',0))
        self.reason = data.get('reason')
        self.name = data.get('name')
        self.modVersion = data.get('modversion')
        self.vrchatVersion = data.get('vrchatversion')
        self.loaderVersion = data.get('loaderversion')
        self.modType: EVRCMGModType = self._fixModType(data.get('modtype'))
        self.author = data.get('author')
        self.description = data.get('description')
        self.downloadLink = data.get('downloadlink')
        self.sourceLink = data.get('sourcelink')
        self.discord = data.get('discord')
        self.hash = base64.b64decode(data['hash']) if 'hash' in data else None
        self.changelog = data.get('changelog')
        self.updateDate = isoparse(data['updatedate'])

    def _fixModType(self, i: str) -> str:
        oi = i
        i = i.lower()
        if i == 'mod':
            return EVRCMGModType.MOD
        elif i == 'plugin':
            return EVRCMGModType.PLUGIN
        elif i == 'normal':
            log.warning('Some idiot set modtype to %r, changing to "Mod".', oi)
            return EVRCMGModType.MOD
        log.warning('Unexpected modtype %r, changing to "Mod"', i)
        return EVRCMGModType.MOD

    def isApproved(self) -> bool:
        return self.status == EApprovalStatus.APPROVED

    def canImport(self) -> bool:
        return self.downloadLink and self.isApproved()
        
class VRCMGMod:
    def __init__(self) -> None:
        self.id: int = -1
        self.mention: Optional[str] = None
        self.messageID: Optional[int] = None
        self.versionOfMsg: Optional[int] = None
        self.uploadDate: datetime.datetime = None
        self.aliases: List[str] = []
        self.versions: List[VRCMGModVersion] = []

    def deserialize(self, data: dict) -> None:
        '''
        "_id": 33,
        "mention": "<REDACTED>",
        "messageid": 827730926959722516,
        "versionofmsg": 8,
        "uploaddate": "2020-04-18T18:35:59.673Z",
        "aliases": [
        "OwO Mod"
        ],
        "versions": [...]
        '''
        self.id = data['_id']
        self.mention = data.get('mention')
        self.messageID = data.get('messageid')
        self.versionOfMsg = data.get('versionofmsg')
        self.aliases = data.get('aliases', [])
        self.uploadDate = isoparse(data['uploaddate'])
        self.versions = {}
        for vd in data['versions']:
            v = VRCMGModVersion(self)
            v.deserialize(vd)
            if v.canImport():
                self.versions[v.modVersion] = v
            else:
                log.warning('Cannot import %r %r: downloadLink=%r, ApprovalStatus=%r', self.id, v.modVersion, v.downloadLink, v.status)

    def getLatestVersion(self) -> Optional[VRCMGModVersion]:
        latestV: Optional[VRCMGModVersion] = None
        for version in self.versions.values():
            if latestV is None or latestV.updateDate < version.updateDate:
                latestV = version
        return latestV   

class VRCMGAPI:
    def __init__(self) -> None:
        self._mods: Optional[Dict[int, VRCMGMod]] = None
        self._modNameLookup: Optional[Dict[str, int]] = {}
        self._last_request_hit_cache: bool = False

    def getModByID(self, id: int) -> VRCMGMod:
        self.getAllMods()
        return self._mods[id]
    def getModByName(self, name: str) -> VRCMGMod:
        self.getAllMods()
        id: int =  self._modNameLookup[name.lower()]
        return self._mods[id]

    def selectEndpoint(self) -> str:
        return random.choice(ENDPOINTS)

    def getAllMods(self, cache: bool = True) -> Dict[str, VRCMGMod]:
        if self._mods:
            return self._mods

        data: dict
        uri = f'{self.selectEndpoint()}/mods.json'
        headers = {'User-Agent': f'VRCTool/{VERSION}'}
        #self.getRateLimiter(uri).delayUntilUsable()
        try:
            if cache:
                cachePath = Path('.cache') / 'vrcmg' / 'mods.json'
                if not cachePath.parent.is_dir():
                    cachePath.parent.mkdir(parents=True)
                self._last_request_hit_cache = not http.downloadToIfModified(uri, str(cachePath), headers=headers)
                with cachePath.open('r', encoding='utf-8') as f:
                    data = json.load(f)
            else:
                self._last_request_hit_cache = False
                res = requests.get(uri, headers=headers)
                res.raise_for_status()
                data = res.json()
        finally:
            #if not self._last_request_hit_cache:
            #    self.getRateLimiter(uri).pushEvent()
            pass

        self._mods = {}
        for entry in data:
            mod = VRCMGMod()
            mod.deserialize(entry)
            self._mods[mod.id] = mod
            for alias in mod.aliases:
                self._modNameLookup[alias.lower()]=mod.id
        return self._mods

