import os
import sys
from pathlib import Path
from typing import List, Optional

from vrctool.vdf import VDFFile

from vrctool.logging import getLogger #isort: skip
log = getLogger(__name__)

def ifDirExistsElse(data: dict, key: str, default: Path) -> Path:
    if key in data:
        p = Path(data[key])
        if p.is_dir():
            return p
    return default

class Paths:
    BASEDIR: Path
    LOCAL_PACKAGES_DIR: Path

    LOCAL_APPDATA_LOW: Path
    VRC_INSTALL_DIR: Path
    VRC_APPDATA_DIR: Path
    GLOBAL_GAME_MANAGERS_FILE: Path
    RESOURCES_ASSETS_FILE: Path

    MOD_DIR: Path
    PLUGIN_DIR: Path

    CONFIG_DIR: Path
    CONFIG_FILE: Path
    REPOS_DIR: Path

    IS_COMPILED = False

    @classmethod
    def Setup(cls, data: dict = {}) -> None:
        cls.IS_COMPILED = '__compiled__' in globals()
        if cls.IS_COMPILED:
            cls.BASEDIR = Path(sys.argv[0]).parent
        else:
            cls.BASEDIR = Path(__file__).parent.parent
            
        cls.BASEDIR = cls.BASEDIR.absolute()
        cls.LOCAL_PACKAGES_DIR = Path.cwd() / 'packages.local'
        cls.TMP_DIR = cls.BASEDIR / 'tmp'

        if os.name == 'nt':
            cls.LOCAL_APPDATA_LOW = Path(os.environ.get('LOCALAPPDATA')+'Low')
            assert cls.LOCAL_APPDATA_LOW.is_dir(), f'%LOCALAPPDATA%Low/ ({cls.LOCAL_APPDATA_LOW}) is not a directory.'

            cls.VRC_LOCAL_LOW = cls.LOCAL_APPDATA_LOW / 'VRChat' / 'vrchat'
            assert cls.VRC_LOCAL_LOW.is_dir(), f'%LOCALAPPDATA%Low/VRChat/vrchat/ ({cls.VRC_LOCAL_LOW}) is not a directory.'

        cls.CONFIG_DIR = Path.home() / '.vrctool'
        cls.CONFIG_FILE = cls.CONFIG_DIR / 'config.json'
        cls.REPOS_DIR = cls.CONFIG_DIR / 'repos.d'

        vrcdir: Optional[str] = data.get('paths', {}).get('vrchat') or os.environ.get('VRCHAT_DIR') or cls.findGame('VRChat')
        if vrcdir is None:
            sys.exit(1)

        cls.VRC_INSTALL_DIR = ifDirExistsElse(data, 'vrchat', Path(vrcdir))
        cls.MOD_DIR = Path(data.get('mods', cls.VRC_INSTALL_DIR / 'Mods'))
        cls.PLUGIN_DIR = Path(data.get('plugins', cls.VRC_INSTALL_DIR / 'Plugins'))
        cls.USERDATA_DIR = Path(data.get('userdata', cls.VRC_INSTALL_DIR / 'UserData'))
        cls.GLOBAL_GAME_MANAGERS_FILE = cls.VRC_INSTALL_DIR / 'VRChat_Data' / 'globalgamemanagers'
        cls.RESOURCES_ASSETS_FILE = cls.VRC_INSTALL_DIR / 'VRChat_Data' / 'resources.assets'

    @classmethod
    def Serialize(cls) -> dict:
        return {
            'vrchat': str(cls.VRC_INSTALL_DIR),
            'mods': str(cls.MOD_DIR),
            'plugins': str(cls.PLUGIN_DIR),
            'userdata': str(cls.USERDATA_DIR),
        }

    @classmethod
    def findGame(cls, gameName) -> Optional[Path]:
        if os.name != 'nt':
            log.warning('Your platform is not supported by VRCTool.  You will need to specify VRChat\'s install directory in $HOME/.vrctool.json or the environment variable VRCHAT_DIR')
            return None
        with log.debug('Searching for %s install directory...', gameName):
            #steamcli = SteamClient()
            # C:\\Program Files (x86)\\Steam\\SteamApps\\libraryfolders.vdf
            library_folders: List[Path] = [
                Path('C:\\Program Files (x86)\\Steam')
            ]
            libfoldervdf = os.path.abspath(os.path.join(library_folders[0], 'steamapps', 'libraryfolders.vdf'))
            if not os.path.isfile(libfoldervdf):
                log.warn('Unable to locate libraryfolders.vdf (Expected: %s)', libfoldervdf)
            else:
                log.debug('Reading %s...', libfoldervdf)
                vdfdata = {}
                vdfdata = VDFFile.LoadFile(libfoldervdf).toDict()
                '''
                "libraryfolders"
                {
                    "contentstatsid"		"-##############"
                    "1"
                    {
                        "path"		"F:\\SteamLibrary"
                        "label"		""
                        "contentid"		"##################"
                        "totalsize"		"1500299390976"
                        "apps"
                        {
                            "220"		"######"
                '''
                key: str
                for key, folderdata in vdfdata['libraryfolders'].items():
                    if key.isdecimal():
                        library_folders.append(Path(folderdata['path']))

            for searchpath in library_folders:
                checkpath = searchpath / 'steamapps' / 'common' / gameName
                if checkpath.is_dir():
                    log.debug('FOUND at %s', checkpath)
                    return checkpath
            log.warning('Unable to autodetect VRChat\'s install directory. You will need to specify its location in %s', cls.CONFIG_FILE)
            return None
