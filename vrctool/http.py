from __future__ import print_function

import json
import logging
import os
from pathlib import Path

from tqdm import tqdm

from vrctool.logging import getLogger #isort: skip
log = getLogger(__name__)

import requests

HTTP_METHOD_GET = 'GET'
HTTP_METHOD_POST = 'POST'


def DownloadFile(url, filename, log_after=True, print_status=True, log_before=True, **kwargs):
    '''
    Download a file from url to filename.

    :param url:
        HTTP URL to download. (SSL/TLS will also work, assuming the cert isn't broken.)
    :param filename:
        Path of the file to download to.
    :param log_after:
        Produce a log statement after the download completes (includes URL).
    :param log_before:
        Produce a log statement before the download starts.
    :param print_status:
        Prints live-updated status of the download progress. (May not work very well for piped or redirected output.)
    :param session:
        Requests session.
    '''
    #kwargs['headers'] = dict(DEFAULT_HEADERS, **kwargs.get('headers', {}))
    r = None
    session = kwargs.pop('session', requests)
    try:
        r = session.get(url, stream=True, **kwargs)
    except requests.exceptions.ConnectionError as e:
        logging.warning(e)
        return False
    except UnicodeEncodeError as e:
        logging.warning(e)
        return False

    if (r.status_code == 404):
        logging.warn('404 - Not Found: %s', url)
        return False
    elif(r.status_code != 200):
        logging.warn("Error code: {0}".format(r.status_code))
        return False
    if not os.path.isdir(os.path.dirname(filename)):
        os.makedirs(os.path.dirname(filename))
    with open(filename, 'wb') as f:
        file_size = int(r.headers.get('Content-Length', '-1'))
        if file_size < 0:
            if 'Content-Length' not in r.headers:
                log.warn('Content-Length header was not received, expect progress bar weirdness.')
            else:
                log.warn('Content-Length header has invalid value: %r', r.headers['Content-Length'])
        if log_before:
            log.info("Downloading: %s Bytes: %s" % (filename, file_size if file_size > -1 else 'UNKNOWN'))

        file_size_dl = 0
        block_sz = 8192

        progress = tqdm(total=file_size, unit_scale=True, unit_divisor=1024, unit='B', leave=False) if print_status else None
        for buf in r.iter_content(block_sz):
            if not buf or file_size == file_size_dl:
                break

            buf_len = len(buf)
            file_size_dl += buf_len
            f.write(buf)
            if print_status:
                #status = r"%10d/%10d  [%3.2f%%]" % (file_size_dl, file_size, file_size_dl * 100. / file_size)
                progress.update(buf_len)
                #status = status + chr(8) * (len(status) + 1)  - pre-2.6 method
                #print(status, end='\r')
        if progress is not None:
            progress.close()
    if log_after:
        log.info('Downloaded %s to %s (%dB)', url, filename, file_size_dl)


METAFILE = Path.cwd() / '.http-headers'
METADATA = None

def fixRequestHeaders(headers: dict, uri: str, destination: str = '') -> None:
    global METADATA

    if METADATA is None:
        METADATA = {}
        if METAFILE.is_file():
            with METAFILE.open('r') as f:
                METADATA = json.load(f)

    mdkey = '$'.join([uri, str(destination)])
    if mdkey in METADATA.keys() and not os.path.isfile(destination):
        log.info('File %s does not exist! Skipping fixRequestHeaders.', destination)
        return
    if mdkey in METADATA:
        if 'lm' in METADATA[mdkey]:
            headers['If-Modified-Since'] = METADATA[mdkey]['lm']
        elif 'etag' in METADATA[mdkey]:
            headers['If-None-Match'] = METADATA[mdkey]['etag']

def updateMetadata(uri, destination, res) -> None:
    global METADATA
    mdkey = '$'.join([uri, str(destination)])
    METADATA[mdkey] = {}
    if 'Last-Modified' in res.headers:
        METADATA[mdkey]['lm'] = res.headers['Last-Modified']
    if 'ETag' in res.headers:
        METADATA[mdkey]['etag'] = res.headers['ETag']
    METADATA[mdkey]['sz'] = int(res.headers.get('Content-Length', '-1'))
    with METAFILE.open('w') as f:
        json.dump(METADATA, f, indent=2)

def requestIfModified(uri: str, tofilename: str = None, params={}, headers={}):
    fixRequestHeaders(headers, uri, tofilename or '')
    res = requests.get(uri, headers=headers, params=params, stream=True)
    if res.status_code == 304:
        log.info('Got HTTP 304 - Not Modified, skipping.')
        return None
    res.raise_for_status()
    updateMetadata(uri, tofilename or '', res)
    return res


def downloadToIfModified(uri: str, filename: str, params={}, headers={}) -> bool:
    if not os.path.isdir(os.path.dirname(filename)):
        os.makedirs(os.path.dirname(filename))
    res = requestIfModified(uri, tofilename=filename, headers=headers, params=params)
    if res is None:
        return False # Not modified
    with open(filename, 'wb') as f:
        file_size = int(res.headers.get('Content-Length', '-1'))
        if file_size < 0:
            if 'Content-Length' not in res.headers:
                log.warn('Content-Length header was not received, expect progress bar weirdness.')
            else:
                log.warn('Content-Length header has invalid value: %r', res.headers['Content-Length'])
        log.info("Downloading: %s Bytes: %s" % (filename, file_size if file_size > -1 else 'UNKNOWN'))

        file_size_dl = 0
        block_sz = 8192

        progress = tqdm(total=file_size, unit_scale=True, unit_divisor=1024, unit='B', leave=False)
        for buf in res.iter_content(block_sz):
            if not buf or file_size == file_size_dl:
                break

            buf_len = len(buf)
            file_size_dl += buf_len
            f.write(buf)
            #status = r"%10d/%10d  [%3.2f%%]" % (file_size_dl, file_size, file_size_dl * 100. / file_size)
            progress.update(buf_len)
            #status = status + chr(8) * (len(status) + 1)  - pre-2.6 method
            #print(status, end='\r')
        if progress is not None:
            progress.close()
    log.info('Downloaded %s to %s (%dB)', uri, filename, file_size_dl)
    return True # Modified
