from base64 import b64decode, b64encode
from enum import IntEnum
import hashlib
import json
import lzma
import os
from pathlib import Path
from typing import Dict, Optional
from vrctool.consts import VERSION

import pgpy
import requests
from semantic_version import Version

from vrctool.http import downloadToIfModified
from vrctool.utils import getFilenameFromURL

from vrctool.logging import getLogger #isort: skip
log = getLogger(__name__)

TMP_DIR = Path('tmp')
class Repository:
    def __init__(self) -> None:
        self.id: str = ''
        self.name: str = ''
        self.url: str = ''
        self.maintainers: dict = {}
        self.key: pgpy.PGPKey = None
        self.keyfile: Optional[Path] = None

    @classmethod
    def FromURL(cls, url: str, key_url: str = None) -> 'Repository':
        repo = cls()
        repo.id = '__TEMP__'
        repo.url = url
        newkeyfile = TMP_DIR / 'newkey.pub'
        key_url = key_url or os.path.dirname(url) + 'repo.pub'
        with log.info('Attempting to load key (%s)...', key_url):
            try:
                res = requests.get(key_url, stream=True)
                res.raise_for_status()
                key = pgpy.PGPMessage.from_blob(res.raw.read())
                assert key.is_public
                repo.key = key
                repo.keyfile = newkeyfile
                log.info(f'Got key {str(repo.key.fingerprint)}!')
            except Exception as e:
                log.warning("This repository does not have GPG signatures! This is a security risk.")
                log.exception(e)
        with log.info('Attempting to load repo metadata...'):
            data = repo.fetch()
            repo.id = repo['meta']['id']
            repo.name = repo['meta']['name']
            repo.maintainers = repo['meta']['maintainers']
        return repo

    def deserialize(self, data: dict) -> None:
        self.id = data['id']
        self.name = data['name']
        self.url = data['url']
        if 'key' in data:
            self.key = pgpy.PGPKey.from_blob(data['key'])
        elif 'keyfile' in data:
            self.keyfile = Path(data['keyfile'])
            self.key = pgpy.PGPKey.from_file(data['keyfile'])

    def serialize(self) -> dict:
        o = {
            'id': self.id,
            'name': self.name,
            'url': self.url,
        }
        if self.key:
            if self.keyfile:
                o['keyfile'] = str(self.keyfile)
            else:
                o['key'] = str(self.key.pubkey)
        return o

    def fetch(self) -> None:
        _, ext = os.path.splitext(getFilenameFromURL(self.url))
        fn = TMP_DIR / f'{self.id}.{ext}'
        fnsig = TMP_DIR / f'{self.id}.{ext}.sig'
        
        downloadToIfModified(self.url, fn)

        hasSig = False
        try:
            downloadToIfModified(self.url+'.sig', fnsig)
            hasSig = True
        except:
            log.warning("This repository does not have GPG signatures! This is a security risk.")
        if hasSig:
            sig: pgpy.PGPSignature = pgpy.PGPSignature.from_file(str(fnsig))
            assert sig is not None
            self.key.verify(fn.read_bytes(), sig)
            log.info('PGP file signature verified!')

        if ext == '.json':
            return self._parseJSON(fn)
        elif ext in ('.lz', '.lzma'):
            return self._parseLZMA(fn)

    def _parseJSON(self, fn: Path) -> dict:
        with fn.open('r') as f:
            return json.load(f)

    def _parseLZMA(self, fn: Path) -> dict:
        with lzma.open(str(fn), 'rt') as lz:
            return json.loads(lz.read())

#  Contains version data for self-updates.
class EPlatform(IntEnum):
    WINDOWS = 1
    LINUX   = 2
    SOURCE  = 3

class DownloadEndpoint:
    def __init__(self) -> None:
        self.platform: EPlatform
        self.url: str
        self.hash: bytes

    def deserialize(self, data) -> None:
        self.url = data['url']
        self.hash = b64decode(data['hash'])

    def serialize(self) -> dict:
        return {
            'url': self.url,
            'hash': b64encode(self.hash),
        }

class MainRepository(Repository):
    def __init__(self) -> None:
        super().__init__()
        self.version: Version = VERSION
        self.endpoints: Dict[EPlatform, DownloadEndpoint] = {}

    def deserialize(self, data: dict) -> None:
        self.version = Version(data['vrctool']['version'])
        self.endpoint = {}
        for k, epdata in data['vrctool']['endpoints'].items():
            platform = EPlatform[k]
            ep = DownloadEndpoint()
            ep.platform = platform
            ep.deserialize(epdata)
            self.endpoints[epdata.platform] = epdata
        super().deserialize(data)

    def serialize(self) -> dict:
        o = {
            'id': self.id,
            'name': self.name,
            'url': self.url,
            'version': self.vrctool_version,
            'endpoints': {k.name: v.serialize() for k, v in self.endpoints.items()}
        }
        if self.key:
            if self.keyfile:
                o['keyfile'] = str(self.keyfile)
            else:
                o['key'] = str(self.key.pubkey)
        return o
