
from vrctool.cmd import _register

from vrctool.logging import getLogger #isort: skip
log = getLogger(__name__)


def main():

    import argparse
    argp = argparse.ArgumentParser()
    subp = argp.add_subparsers()

    _register(subp)

    args = argp.parse_args()

    if hasattr(args, 'cmd') and args.cmd is not None:
        args.cmd(args)
    else:
        argp.print_help()

if __name__ == '__main__':
    main()
