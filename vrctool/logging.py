
from enum import IntEnum
import os, re
import logging as _logging
from typing import Dict, List, Optional, Tuple, Union
import colorama

class NullIndenter(object):
    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        return False

# Regular expression used to detect color tags for colorize()
_REGEX_COLOR = re.compile(r'<(red|green|yellow|blue|magenta|cyan|white)>([^<]+)</\1>')

_COLORSTART='\033['
_COLOREND='m'

_COLORS={
    'red':     31,
    'green':   32,
    'yellow':  33,
    'blue':    34,
    'magenta': 35,
    'cyan':    36,
    'white':   37,
}

INDENT = 0

def encodeColor(colorID):
    return _COLORSTART+str(colorID)+_COLOREND

def _colorWrapper(m):
    colorName=m.group(1)
    text = m.group(2)
    colorID=_COLORS[colorName]
    return encodeColor(colorID)+text+encodeColor(0)

def colorize(text):
    return _REGEX_COLOR.sub(_colorWrapper,text)

class IndentLogger(object):
    '''
    Indents stuff.
    '''

    def __init__(self, logger=None):
        self.log = logger
        self.useAnsiColors=False
        if isinstance(self.log, str):
            self.log = _logging.getLogger(self.log)
        if self.log is None:
            self.log = _logging.getLogger()

    def __enter__(self):
        global INDENT
        INDENT += 1
        return self

    def __exit__(self, type, value, traceback):
        global INDENT
        INDENT -= 1
        return False

    def enableANSIColors(self,on=True):
        self.useAnsiColors=on
        if self.useAnsiColors:
            colorama.init(convert=True)
        else:
            colorama.deinit()

    def debug(self, msg, *args, **kwargs):
        """
        Log 'msg % args' with severity 'DEBUG'.

        To pass exception information, use the keyword argument exc_info with
        a true value, e.g.

        logger.debug("Houston, we have a %s", "thorny problem", exc_info=1)
        """
        if self.log.isEnabledFor(_logging.DEBUG):
            self._log(_logging.DEBUG, msg, args, **kwargs)
        return self

    def info(self, msg, *args, **kwargs):
        """
        Log 'msg % args' with severity 'INFO'.

        To pass exception information, use the keyword argument exc_info with
        a true value, e.g.

        logger.info("Houston, we have a %s", "interesting problem", exc_info=1)
        """
        if self.log.isEnabledFor(_logging.INFO):
            self._log(_logging.INFO, msg, args, **kwargs)
        return self

    def warning(self, msg, *args, **kwargs):
        """
        Log 'msg % args' with severity 'WARNING'.

        To pass exception information, use the keyword argument exc_info with
        a true value, e.g.

        logger.warning("Houston, we have a %s", "bit of a problem", exc_info=1)
        """
        if self.log.isEnabledFor(_logging.WARNING):
            self._log(_logging.WARNING, msg, args, **kwargs)
        return self

    warn = warning

    def error(self, msg, *args, **kwargs):
        """
        Log 'msg % args' with severity 'ERROR'.

        To pass exception information, use the keyword argument exc_info with
        a true value, e.g.

        logger.error("Houston, we have a %s", "major problem", exc_info=1)
        """
        if self.log.isEnabledFor(_logging.ERROR):
            self._log(_logging.ERROR, msg, args, **kwargs)
        return self

    def exception(self, msg, *args, **kwargs):
        """
        Convenience method for logging an ERROR with exception information.
        """
        kwargs['exc_info'] = 1
        self.error(msg, *args, **kwargs)
        return self

    def critical(self, msg, *args, **kwargs):
        """
        Log 'msg % args' with severity 'CRITICAL'.

        To pass exception information, use the keyword argument exc_info with
        a true value, e.g.

        logger.critical("Houston, we have a %s", "major disaster", exc_info=1)
        """
        if self.log.isEnabledFor(_logging.CRITICAL):
            self._log(_logging.CRITICAL, msg, args, **kwargs)
        return self

    def _log(self, level, msg, args, exc_info=None, extra=None):
        if self.useAnsiColors:
            msg=colorize(msg)
        if isinstance(msg, str):
            indent = INDENT * '  '
            self.log._log(level, indent + msg, args, exc_info, extra)
        else:
            self.log._log(level, msg, args, exc_info, extra)

#def basicConfig(level=_logging.INFO):
_logging.basicConfig(
    format='%(asctime)s [%(levelname)-8s]: %(message)s',
    datefmt='%m/%d/%Y %I:%M:%S %p',
    level=_logging.INFO)
    # filename='logs/main.log',
    # filemode='w')

def logToFile(logID, mode='w', level=_logging.INFO, sub_dir=None, start_message='Logging started', announce_location=False, formatter=None):
    basedir = 'logs'
    if sub_dir is not None:
        basedir = os.path.join(basedir, sub_dir)
    if not os.path.isdir(basedir):
        os.makedirs(basedir)
    logfile = os.path.join(basedir, logID + '.log')
    log = _logging.getLogger(logID)
    _logging.info('Opening %s log at %s (mode: %s)...',logID,logfile,mode)
    if len(log.handlers) == 0:
        # if os.path.isfile(logfile):
        #    os.remove(logfile)
        console = _logging.FileHandler(logfile, mode=mode)
        console.setLevel(level)
        if formatter:
            console.setFormatter(formatter)
        log.addHandler(console)
    if start_message is not None:
        log.info(start_message)
    return log

# define a Handler which writes INFO messages or higher to the sys.stderr
# console = _logging.StreamHandler()
# console.setLevel(_logging.INFO)
# _logging.getLogger('').addHandler(console)

def getLogger(name: str) -> _logging.Logger:
    l: _logging.Logger = _logging.getLogger(name)
    return IndentLogger(l)


class EMessageType(IntEnum):
    INFO = _logging.INFO
    WARNING = _logging.WARNING
    ERROR = _logging.ERROR

class PostMessagingBox:
    def __init__(self) -> None:
        self.messages: Dict[str, List[Tuple[EMessageType, str]]] = {}

        self.current_package: Optional[str] = None

    def _add(self, pkgid: str, msgtype: EMessageType, message: str) -> None:
        if pkgid not in self.messages:
            self.messages[pkgid] = []
        self.messages[pkgid].append((msgtype, message))
    def addInfo(self, message: str, pkgid: Optional[str] = None) -> None:
        self._add(pkgid or self.current_package, EMessageType.INFO, message)
    def addWarning(self, message: str, pkgid: Optional[str] = None) -> None:
        self._add(pkgid or self.current_package, EMessageType.WARNING, message)
    def addError(self, message: str, pkgid: Optional[str] = None) -> None:
        self._add(pkgid or self.current_package, EMessageType.Error, message)

    def toLog(self, log: Union[_logging.Logger,IndentLogger]) -> None:
        for pkgid, msgs in self.messages.items():
            with log.info(f'In {pkgid}:'):
                for msgtype, message in msgs:
                    if isinstance(log, _logging.Logger):
                        #log: _logging.Logger
                        log.log(msgtype.value, message)
                    if isinstance(log, IndentLogger):
                        log._log(msgtype.value, message, [])

    def forPackage(self, pkgid: str):
        self.current_package = pkgid
        return self

    def __enter__(self):
        return self
    def __exit__(self, type, value, traceback):
        self.current_package = None
        return False
