import argparse
import tqdm
import sys
import os
from vrctool.consts import CURRENT_GAME_BUILD, CURRENT_GAME_VERSION, VERSION, NAME, SITE_URL
from pathlib import Path
from vrctool.paths import Paths
from vrctool.utils import sizeof_fmt, remove_empty_folders

from vrctool.logging import getLogger  # isort: skip
log = getLogger(__name__)


def register__version(subp) -> None:
    versionp = subp.add_parser('version')
    versionp.add_argument('--format', '-f', choices=['text', 'yaml', 'json', 'toml'], default='text')
    versionp.set_defaults(cmd=cmd_version)


def cmd_version(args: argparse.Namespace) -> None:
    prefix, major, minor, patch, p, githash = CURRENT_GAME_VERSION
    githash = githash.to_bytes((githash.bit_length()+7) // 8, 'big').hex()
    if args.format == 'text':
        print(f'{NAME}: VRChat Swiss Army Knife')
        print('Copyright (c)2021 VRCTool Contributors')
        print('Licensed to you under the MIT Open Source License')
        print(f'Source and Binaries: {SITE_URL}')
        print(f'Version: {VERSION}')
        print(f'Built for VRChat Build {CURRENT_GAME_BUILD} ({prefix}{major}.{minor}.{patch}p{p}-{githash})')
        return
    data = {
        'vrctool': {
            'major': VERSION.major,
            'minor': VERSION.minor,
            'patch': VERSION.patch,
            'build': VERSION.build,
            'prerelease': VERSION.prerelease,
            'str': str(VERSION)
        },
        'vrchat': {
            'build': CURRENT_GAME_BUILD,
            'version': {
                'prefix': prefix,
                'major': major,
                'minor': minor,
                'patch': patch,
                'p': p,
                'githash': githash,
                'str': f'w_{major}.{minor}.{patch}p{p}-{githash}'
            }
        }
    }
    '''
    AAAAAAAAAAAAAAAAAAAAAAA python 3.10 required
    match args.format:
        case 'yaml':
            from ruamel.yaml import YAML
            print(YAML(typ='rt').dump(data))
        case 'json':
            import json
            print(json.dumps(data))
        case 'toml':
            import toml
            print(toml.dumps(data))
    '''
    if args.format == 'yaml':
        from ruamel.yaml import YAML
        YAML(typ='rt').dump(data, sys.stdout)
    elif args.format == 'json':
        import json
        print(json.dumps(data))
    elif args.format == 'toml':
        import toml
        print(toml.dumps(data))
