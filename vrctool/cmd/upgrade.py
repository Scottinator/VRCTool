from vrctool.state import State
import argparse

from vrctool.logging import getLogger # isort: skip
log = getLogger(__name__)

def register__upgrade(subp) -> None:
    upgradep = subp.add_parser('upgrade')
    upgradep.set_defaults(cmd=cmd_upgrade)

def cmd_upgrade(args: argparse.ArgumentParser) -> None:
    state = State()
    state.upgrade()
