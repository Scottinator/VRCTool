from vrctool.state import State
import argparse

from vrctool.logging import getLogger # isort: skip
log = getLogger(__name__)

def register__install(subp) -> None:
    installp = subp.add_parser('install')
    installp.add_argument('--force-loader-reinstall', action='store_true', default=False)
    installp.set_defaults(cmd=cmd_install)

def cmd_install(args: argparse.ArgumentParser) -> None:
    state = State()
    state.install(args.force_loader_reinstall)
