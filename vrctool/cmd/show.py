import argparse
import binascii
import json
import hashlib
from vrctool.consts import ML_HASHES, VRC_HASHES
from ruamel.yaml import YAML
from pathlib import Path
from vrctool.paths import Paths
from vrctool.utils import checksum, sizeof_fmt, remove_empty_folders
from vrctool.vrcdata import getVRCVersion
from vrctool.logging import getLogger # isort: skip
log = getLogger(__name__)

yaml = YAML(typ='rt')

def register__show(subp) -> None:
    showp = subp.add_parser('show')

    showsubp = showp.add_subparsers()

    _register_show_paths(showsubp)
    _register_show_vrchat(showsubp)

def _register_show_paths(cachesubp) -> None:
    pathsp = cachesubp.add_parser('paths')
    pathsp.add_argument('--format', choices=['text', 'yaml', 'json'], default='text')
    pathsp.set_defaults(cmd=cmd_show_paths)
def cmd_show_paths(args: argparse.Namespace) -> None:
    Paths.Setup()
    o = {
        'Paths': {
            'BASEDIR': Paths.BASEDIR,
            'LOCAL_PACKAGES_DIR': Paths.LOCAL_PACKAGES_DIR,
            'CONFIG_FILE': Paths.CONFIG_FILE,

            'VRC_INSTALL_DIR': Paths.VRC_INSTALL_DIR,
            'MOD_DIR': Paths.MOD_DIR,
            'PLUGIN_DIR': Paths.PLUGIN_DIR,
            'USERDATA_DIR': Paths.USERDATA_DIR,
            'GLOBAL_GAME_MANAGERS': Paths.GLOBAL_GAME_MANAGERS_FILE,
            'RESOURCES_ASSETS_FILE': Paths.RESOURCES_ASSETS_FILE,

            'LOCAL_APPDATA_LOW': Paths.LOCAL_APPDATA_LOW,
            'VRC_LOCAL_LOW': Paths.VRC_LOCAL_LOW,
        }
    }
    keyLen = 20
    if args.format == 'text':
        for secname, section in o.items():
            print(f'{secname}:')
            keyLen = max([len(k) for k in section.keys()])
            for k, v in section.items():
                kf = f'{k}:'
                if isinstance(v, Path):
                    v = str(v)
                print(f'  {kf:<{keyLen+2}}{v}')
    if args.format == 'yaml':
        from io import StringIO
        s = StringIO()
        yaml.dump(o, s)
        print(s.buffer)
    elif args.format == 'json':
        print(json.dumps(o))
    


def _register_show_vrchat(cachesubp) -> None:
    vrcp = cachesubp.add_parser('vrchat', aliases=['vrc'], help='Retrieve information about your VRChat installation')
    vrcp.add_argument('--format', choices=('text', 'json', 'yaml'), default='text', help='How to display the data')
    vrcp.add_argument('--for-build', action='store_true', default=False, help='Whether to structure the data for VRCTool\'s development environment')
    vrcp.set_defaults(cmd=cmd_show_vrchat)
def cmd_show_vrchat(args: argparse.Namespace) -> None:
    Paths.Setup()
    v = getVRCVersion()
    if args.for_build and args.format == 'text':
        args.format = 'json'
    o = {
        'version': {
            'full': str(v),
            'appVersion': v.getAppVersion(),
            'prefixed': v.getPrefixedVersion(),
            'prefix': v.prefix, 
            'major': v.major, 
            'minor': v.minor, 
            'revision': v.revision, 
            'patch': v.patch, 
            'build': v.build,
            'channel': v.channel.name,
            'hash': v.hash
        },
        'hashes': {}
    }
    MLFILES = [
        Paths.VRC_INSTALL_DIR / 'GameAssembly.dll',
        Paths.VRC_INSTALL_DIR / 'MelonLoader' / 'MelonLoader.dll',
        Paths.VRC_INSTALL_DIR / 'MelonLoader' / 'Dependencies' / 'Bootstrap.dll',
    ]

    if args.for_build:
        o = {
            'prefix': v.prefix, 
            'major': v.major, 
            'minor': v.minor, 
            'revision': v.revision, 
            'patch': v.patch, 
            'build': v.build,
            'channel': v.channel.value,
            'hash': v.hash
        }
    else:
        PROXY_HASH: bytes = ML_HASHES[Path('version.dll')]
        proxyfiles = [
            Path('psapi.dll'),
            Path('version.dll'),
            Path('winhttp.dll'),
            Path('winmm.dll'),
        ]
        for pf in proxyfiles:
            if (Paths.VRC_INSTALL_DIR / pf).is_file():
                del ML_HASHES[Path('version.dll')]
                ML_HASHES[pf] = PROXY_HASH
                break

        failed = []
        for fp, hashbytes in VRC_HASHES.items():
            fp = Paths.VRC_INSTALL_DIR / fp
            rfp = fp.relative_to(Paths.VRC_INSTALL_DIR)
            if fp.is_file():
                mine = checksum(hashlib.sha512, fp).upper()
                known = binascii.b2a_hex(hashbytes).decode('ascii').upper()
                ok = mine==known
                o['hashes'][str(rfp)] = {'user': mine, 'known': known, 'result': ok}
                if not ok:
                    failed += [rfp]
            else:
                o['hashes'][str(rfp)] = None
        for fp, hashbytes in ML_HASHES.items():
            fp = Paths.VRC_INSTALL_DIR / fp
            rfp = fp.relative_to(Paths.VRC_INSTALL_DIR)
            if fp.is_file():
                mine = checksum(hashlib.sha512, fp).upper()
                known = binascii.b2a_hex(hashbytes).decode('ascii').upper()
                ok = mine == known
                o['hashes'][str(rfp)] = {'user': mine, 'known': known, 'result': ok}
                if not ok:
                    failed += [rfp]
            else:
                o['hashes'][str(rfp)] = None

    if args.format == 'text':
        for secname, section in o.items():
            if secname == 'version':
                print(f'VRChat Version:')
            elif secname == 'hashes':
                print(f'File Checksums (SHA512):')
            keyLen = max([len(k) for k in section.keys()])
            for k, v in section.items():
                kf = f'{k.title()}:' if secname != 'hashes' else f'{k}:'
                if isinstance(v, Path):
                    v = str(v)
                if isinstance(v, dict):
                    chk = '✓' if v['result'] else '✗'
                    v = f"{chk} {v['user']}"
                print(f'  {kf:<{keyLen+2}}{v}')
    elif args.format == 'yaml':
        from io import StringIO
        s = StringIO()
        yaml.dump(o, s)
        print(s.buffer)
    elif args.format == 'json':
        print(json.dumps(o))

