# Generated
from .cache import register__cache
from .disable import register__disable
from .enable import register__enable
from .install import register__install
from .uninstall import register__uninstall
from .show import register__show
from .repo import register__repo
from .version import register__version

def _register(subp) -> None:
    register__cache(subp)
    register__disable(subp)
    register__enable(subp)
    register__install(subp)
    register__repo(subp)
    register__show(subp)
    register__uninstall(subp)
    register__version(subp)
