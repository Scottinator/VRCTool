from vrctool.state import State
import argparse

from vrctool.packages import PackageInstallation
from vrctool.paths import Paths
from vrctool.logging import getLogger # isort: skip
log = getLogger(__name__)

def register__disable(subp) -> None:
    disablep = subp.add_parser('disable')
    disablep.add_argument('packages', type=str, nargs='+')
    disablep.set_defaults(cmd=cmd_disable)

def cmd_disable(args: argparse.ArgumentParser) -> None:
    state = State()

    failed=False
    for pkgid in args.packages:
        pkgid = pkgid.lower()
        if pkgid not in state.enabled_packages:
            log.error('Package %r not in %s:/packages.', pkgid, Paths.CONFIG_FILE)
            failed = True
            continue
        with log.info('Removing package %r...', pkgid):
            state.enabled_packages.remove(pkgid)
            installation = PackageInstallation.Get(pkgid)
            if installation is not None:
                installation.uninstall()

    if failed:
        return
    #state.syncMods(data)
    #state.cleanupDeadFiles()
    state.save()
