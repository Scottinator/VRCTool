import argparse
import base64
from io import BytesIO
import struct
import subprocess
from typing import List
import tqdm
import os
import sys
import requests
import lzma
import toml
import shutil
from pathlib import Path
from vrctool.paths import Paths
from vrctool.state import State
from vrctool.consts import ALLOWED_REPOID_CHARS, SELFUPDATE_VERSION
from vrctool.utils import sizeof_fmt, remove_empty_folders
from vrctool.repo import Repository

from vrctool.logging import getLogger  # isort: skip
log = getLogger(__name__)


def register__selfupdate(subp) -> None:
    cmdp = subp.add_parser('self-update', aliases=['su'])
    cmdp.add_argument('--config', type=str, default=None, help='Internal self-updater stuff, as base64-encoded bytes.')
    cmdp.set_defaults(cmd=cmd_selfupdate)


def unpack_int(data: BytesIO) -> int:
    return struct.unpack('>I', data.read(4))
def unpack_str(data: BytesIO) -> str:
    n = unpack_int(data)
    return data.read(n).decode('utf-8')
def unpack_strlist(data: BytesIO) -> List[str]:
    n = unpack_int(data)
    o = []
    for _ in range(n):
        o += unpack_str(data)
    return o

def cmd_selfupdate(args: argparse.Namespace) -> None:
    if not args.config:
        Paths.Setup()
        state = State()
        state._selfUpdate(byCmd=True)
    else:
        data = BytesIO(base64.b64decode(args.config))
        assert unpack_int(data) == SELFUPDATE_VERSION, '--config specified an invalid struct version. You probably need to update vrctool manually.'
        pid = unpack_int(data)
        log.info('VRCTool Process ID: %d', pid)
        cwd = Path(unpack_str(data))
        log.info('Current Working Directory: %s', cwd)
        args = unpack_strlist(data)
        log.info('Command-line args: %r', args)
        installDir = Path(unpack_str(data))
        log.info('Install Directory: %s', installDir)

        log.info('Killing old process #%d', pid)
        os.kill(pid)
        os.chdir(os.path.dirname(sys.argv[0]))
        sucwd = Path.cwd()
        for srcfile in sucwd.rglob('*'):
            if not srcfile.is_file():
                continue
            dstfile = installDir / srcfile.relative_to(sucwd)
            log.info('Copying %s -> %s...', srcfile, dstfile)
            shutil.copy2(srcfile, dstfile)
        log.info('Finished updating files.')
        if len(args):
            with log.info('Launching updated process:'):
                log.info('chdir %r', cwd)
                os.chdir(cwd)
                log.info('execl %r', args)
                os.execl(args[0], *args[:])        
