from vrctool.state import State
import argparse

from vrctool.logging import getLogger # isort: skip
log = getLogger(__name__)

def register__uninstall(subp) -> None:
    uninstallp = subp.add_parser('uninstall')
    uninstallp.add_argument('--all', action='store_true', default=False)
    uninstallp.add_argument('--mods', action='store_true', default=False)
    uninstallp.add_argument('--prefs', action='store_true', default=False)
    uninstallp.set_defaults(cmd=cmd_uninstall)

def cmd_uninstall(args: argparse.ArgumentParser) -> None:
    state = State()
    state.uninstall()
