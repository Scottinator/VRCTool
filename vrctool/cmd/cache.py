import argparse
import tqdm
import os
import sys
import binascii
from pathlib import Path
from vrctool.paths import Paths
from vrctool.utils import sizeof_fmt, remove_empty_folders, del_empty_dirs
from vrctool.unitycache import Cache, CachedAssetBundle

from vrctool.logging import getLogger # isort: skip
log = getLogger(__name__)

def register__cache(subp) -> None:
    cachep = subp.add_parser('cache')

    cachesubp = cachep.add_subparsers()

    _register_cache_clear(cachesubp)
    _register_cache_stats(cachesubp)
    _register_cache_sweep(cachesubp)

def _register_cache_clear(cachesubp) -> None:
    clearp = cachesubp.add_parser('clear')
    clearp.set_defaults(cmd=cmd_cache_clear)

def _register_cache_stats(cachesubp) -> None:
    statsp = cachesubp.add_parser('stats')
    statsp.add_argument('--format', choices=['text', 'json', 'yaml', 'toml'], default='text')
    statsp.add_argument('--human-readable', '-H', action='store_true', default=None)
    statsp.set_defaults(cmd=cmd_cache_stats)

def _register_cache_sweep(cachesubp) -> None:
    sweepp = cachesubp.add_parser('sweep')
    sweepp.set_defaults(cmd=cmd_cache_sweep)

def cmd_cache_sweep(args: argparse.Namespace) -> None:
    Paths.Setup()
    for subdir in ('Cache-WindowsPlayer', 'HTTPCache-WindowsPlayer', 'Unity', 'Cookies'):
        subject = Paths.VRC_LOCAL_LOW / subdir
        with log.info('Removing empty files from %s...', subject):
            del_empty_dirs(str(subject))

    cache = Cache()
    cache.loadFromDir(Paths.VRC_LOCAL_LOW / 'Cache-WindowsPlayer')
    cache.clean()

def cmd_cache_clear(args: argparse.Namespace) -> None:
    Paths.Setup()
    log.info("Collecting files...")
    all_files = []
    for subdir in ('Cache-WindowsPlayer', 'HTTPCache-WindowsPlayer', 'Unity', 'Cookies'):
        subject = Paths.VRC_LOCAL_LOW / subdir
        for root, _, files in os.walk(subject):
            for filen in files:
                all_files.append(Path(os.path.abspath(os.path.join(root, filen))))
    if len(all_files) > 0:
        for filepath in tqdm.tqdm(all_files, desc='Clearing', ascii=True, unit='file'):
            if filepath.is_file():
                filepath.unlink()
    else:
        log.info('Already cleared.')
    log.info('Eliminating empty dirs...')
    for subdir in ('Cache-WindowsPlayer', 'HTTPCache-WindowsPlayer', 'Unity', 'Cookies'):
        remove_empty_folders(str(Paths.LOCAL_APPDATA_LOW / subdir))

def cmd_cache_stats(args: argparse.Namespace) -> None:
    Paths.Setup()
    #log.info("Scanning files...")
    filesize = 0
    filecount = 0
    statsFor = {}
    for subdir in ('Cache-WindowsPlayer', 'HTTPCache-WindowsPlayer', 'Unity', 'Cookies'):
        subject = Paths.VRC_LOCAL_LOW / subdir
        subdirsz = 0
        subdircount = 0
        for root, _, files in os.walk(subject):
            for filen in files:
                sz = os.stat(os.path.abspath(os.path.join(root, filen))).st_size
                filesize += sz
                subdirsz += sz
                filecount += 1
                subdircount += 1
        statsFor[subdir] = (subdirsz, subdircount)
    data = {}
    human_readable: bool = args.format == 'text'
    if args.human_readable is not None:
        human_readable = True
    for subdir, a in statsFor.items():
        sz, count = a
        subject = Paths.VRC_LOCAL_LOW / subdir
        key = f'%LOCALAPPDATA%Low/VRChat/vrchat/{subdir}'
        if not subject.is_dir():
            data[key] = None
            continue
        data[key] = {
            'File Count': f'{count:n}' if human_readable else count,
            'Total Size': f'{sizeof_fmt(sz)}' if human_readable else sz
        }
        if count > 0:
            data[key]['Average Size'] = f'{sizeof_fmt(sz/count)}' if human_readable else (sz/count)

    data['Total'] = {
        'File Count': f'{filecount:n}' if human_readable else filecount,
        'Total Size': f'{sizeof_fmt(filesize)}' if human_readable else filesize,
    }
    if filecount:
        data['Total']['Average Size'] = f'{sizeof_fmt(filesize/filecount)}' if human_readable else (filesize/filecount)

    if args.format == 'text':
        for subdir, stats in data.items():
            print(f'{subdir}:')
            for k, v in stats.items():
                print(f'  {k}: {v}')
    elif args.format == 'json':
        import json
        print(json.dumps(data))
    elif args.format == 'yaml':
        from ruamel.yaml import YAML
        yaml = YAML(typ='safe')
        yaml.default_flow_style = False
        yaml.dump(data, sys.stdout)
    elif args.format == 'toml':
        import toml
        print(toml.dumps(data))
