from vrctool.state import State
import argparse

from vrctool.paths import Paths

from vrctool.logging import getLogger # isort: skip
log = getLogger(__name__)

def register__enable(subp) -> None:
    enablep = subp.add_parser('enable')
    enablep.add_argument('packages', type=str, nargs='+')
    enablep.set_defaults(cmd=cmd_enable)

def cmd_enable(args: argparse.ArgumentParser) -> None:
    state = State()

    failed=False
    for pkgid in args.packages:
        pkgid=pkgid.lower()
        if pkgid not in state.all_packages:
            log.error('%r not in repository packages.', pkgid)
            failed = True
        if pkgid in state.enabled_packages:
            log.error('%r already in %s:/packages.', pkgid, Paths.CONFIG_FILE)
            #failed = True

    if failed:
        return
    state.enabled_packages = list(set(state.enabled_packages+[f.lower() for f in args.packages]))
    state.syncMods()
    state.cleanupDeadFiles()
    state.save()
