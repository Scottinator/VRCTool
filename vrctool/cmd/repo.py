import argparse
import tqdm
import os
import sys
import requests
import lzma
import toml
import pgpy
from pathlib import Path
from vrctool.paths import Paths
from vrctool.state import State
from vrctool.consts import ALLOWED_REPOID_CHARS
from vrctool.utils import sizeof_fmt, remove_empty_folders
from vrctool.repo import Repository

from vrctool.logging import getLogger # isort: skip
log = getLogger(__name__)

def register__repo(subp) -> None:
    repop = subp.add_parser('repo')

    reposubp = repop.add_subparsers()

    _register_repo_add(reposubp)
    _register_repo_list(reposubp)
    _register_repo_rm(reposubp)

def _register_repo_add(reposubp) -> None:
    addp = reposubp.add_parser('add')
    addp.add_argument('--id', type=str, default=None, help='Override repo ID.')
    addp.add_argument('--allow-overwrite', action='store_true', default=False, help='Allow overwriting an existing repo.')
    addp.add_argument('uri', help='URL of packages.lz for the repository.')
    addp.set_defaults(cmd=cmd_repo_add)

def cmd_repo_add(args: argparse.Namespace) -> None:
    Paths.Setup()
    state = State()
    repo = Repository.FromURL(args.uri)
    repo.id = repo.id.lower()
    if args.id:
        log.info('ID: %r -> %r', repo.id, args.id.lower())
        repo.id = args.id.lower()
    if all([c in ALLOWED_REPOID_CHARS for c in repo.id]):
        log.error('Invalid repository ID: %r', repo.id)
        log.error('Allowed chars: %r', ALLOWED_REPOID_CHARS)
        return
    if repo.id.lower() in state.repos:
        if args.allow_overwrite:
            log.warning('Repo ID %r will overwrite another repo by the same name.', repo.id)
        else:
            log.error('Repo ID %r will overwrite another repo by the same name.', repo.id)
            return
    tomlfile = Path('repos.d') / f'{repo.id}.toml'
    if repo.key:
        repo.keyfile = Path('repos.d') / f'{repo.id}.pub'
    with tomlfile.open('w') as f:
        toml.dump(repo.serialize(), f)
        log.info('Wrote %s to %s.', sizeof_fmt(os.path.getsize(tomlfile)), tomlfile)
    if repo.key:
        repo.keyfile.write_text(str(repo.key))
        log.info('Wrote %s to %s.', sizeof_fmt(os.path.getsize(repo.keyfile)), repo.keyfile)

def _register_repo_rm(reposubp) -> None:
    rmp = reposubp.add_parser('remove', aliases=['rm'])
    rmp.add_argument('id', help='Unique name of the package repository.')
    rmp.set_defaults(cmd=cmd_repo_rm)

def cmd_repo_rm(args: argparse.Namespace) -> None:
    Paths.Setup()
    state = State()
    tomlfile = Path('repos.d') / f'{args.id.lower()}.toml'
    if not tomlfile.is_file():
        log.error('Repo %r does not exist (%s)!', args.id.lower(), tomlfile)
        return
    tomlfile.unlink
    log.info('Saved!')

def _register_repo_list(reposubp) -> None:
    listp = reposubp.add_parser('list', aliases=['ls'])
    listp.set_defaults(cmd=cmd_repo_list)
def cmd_repo_list(args: argparse.Namespace) -> None:
    state = State()
    for repoid, repo in state.repos.items():
        with log.info('%s:', repoid):
            log.info('id:       %r', repo.id)
            log.info('name:     %r', repo.name)
            log.info('url:      %r', repo.url)
            log.info('keyfile:  %r', repo.keyfile)
            log.info('key:      %s', repo.key.fingerprint if repo.key else 'None')
