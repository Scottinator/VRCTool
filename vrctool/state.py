import binascii
import hashlib
import json
import os
import shutil
import sys
import logging
from enum import IntEnum
from vrctool.packages.base import PackageVersion
from vrctool.http import downloadToIfModified
import zipfile
from pathlib import Path
from typing import Any, Dict, List, Optional, Set, Tuple

import pgpy
import requests
import toml
from tempfile import TemporaryDirectory

from vrctool.consts import (CURRENT_GAME_BUILD, FILES_TO_NUKE_FIRST,
                            FILES_TOUCHED, MAIN_REPO_ID, MAIN_REPO_NAME,
                            MAIN_REPO_PUBKEY, MAIN_REPO_URI,
                            MIN_ACCEPTABLE_GAME_BUILD, ML_HASHES, PACKAGE_SCHEMA_VERSION, SELFUPDATE_VERSION, VERSION)
from vrctool.packages import (EPackageType, GitHubPackage, Package)
from vrctool.paths import Paths
from vrctool.repo import DownloadEndpoint, EPlatform, MainRepository, Repository
from vrctool.utils import checksum
from vrctool.vrcdata import VRChatChannel, VRChatVersion, getVRCVersion

from vrctool.logging import PostMessagingBox, getLogger #isort: skip
log = getLogger(__name__)

MELONLOADER_SHA512 = GitHubPackage('melonloader.hash', EPackageType.OTHER, 'LavaGang', 'MelonLoader', 'MelonLoader.x64.sha512')
MELONLOADER_ZIP    = GitHubPackage('melonloader.zip',  EPackageType.OTHER, 'LavaGang', 'MelonLoader', 'MelonLoader.x64.zip')

CONFIG_VERSION: int = 1

SCRIPT_DIR = Path(sys.argv[0]).parent() if '__compiled__' in globals() else Path.cwd()

class State:
    def __init__(self, load_config: bool = True, load_mods: bool = True) -> None:
        self.all_packages: Dict[str, Package] = {}
        self.enabled_packages: List[str] = []
        self.config: Dict[str, Any] = {}
        self.repos: Dict[str, Repository] = {}

        self.game_version: Optional[VRChatVersion] = None

        mainrepo = MainRepository()
        mainrepo.id = MAIN_REPO_ID
        mainrepo.name = MAIN_REPO_NAME
        mainrepo.url = MAIN_REPO_URI
        mainrepo.key, _ = pgpy.PGPKey.from_blob(MAIN_REPO_PUBKEY)
        self.main_repo = mainrepo
        self.repos[mainrepo.id] = mainrepo

        #MELONLOADER_ZIP.files['']

        if load_config:
            self.loadConfig()
        if load_mods:
            self.loadMods()

    def loadConfig(self, override_path: Optional[Path] = None) -> None:
        if override_path is None:
            old_config = Path.home() / '.vrctool.json'
            Paths.CONFIG_DIR = Path.home() / '.vrctool'
            if not Paths.CONFIG_DIR.is_dir():
                Paths.CONFIG_DIR.mkdir(mode=0o600)
            Paths.CONFIG_FILE =  Paths.CONFIG_DIR / 'config.json'

            if old_config.is_file():
                log.warning(f'Configuration has been moved from {old_config} to {Paths.CONFIG_FILE}.')
                os.rename(old_config, Paths.CONFIG_FILE)

            if not Paths.CONFIG_FILE.is_file():
                log.info(f'Writing new config file ({Paths.CONFIG_FILE})...')
                configexample = {
                    'paths': {
                        'vrchat': Paths.findGame('VRChat'),
                    },
                    'loader': {
                        'zip': MELONLOADER_ZIP.serialize(), # v0.3.0
                        'sha512': MELONLOADER_SHA512.serialize(), # v0.3.0
                    },
                    'packages': [],
                }

                with Paths.CONFIG_FILE.open('w') as f:
                    json.dump(configexample, f, indent=2)

        configpath: Path = override_path or Paths.CONFIG_FILE

        data = None
        with configpath.open('r') as f:
            data = json.load(f)
            Paths.Setup(data.get('paths', {}))
            self.enabled_packages = [x.lower() for x in data.get('packages', data.get('mods', []))]

            if 'loader' in data:
                if 'zip' in data['loader']:
                    MELONLOADER_ZIP.deserialize(data['loader']['zip'])
                if 'sha512' in data['loader']:
                    MELONLOADER_SHA512.deserialize(data['loader']['sha512'])
        self.config = data

        MELONLOADER_ZIP.install_path = Path('tmp') / 'loader.zip'

        for repotoml in Path('repos.d').glob('*.toml'):
            with repotoml.open('r') as f:
                data = toml.load(f)
                repo = Repository()
                repo.deserialize(data)
                self.repos[repo.id] = repo

    def loadMods(self) -> None:
        self.all_packages = {}

        for repo in self.repos.values():
            with log.info('Loading from repo %s...', repo.id):
                data = repo.fetch()
                if not data:
                    log.error('data == None')
                    continue
                if data['schema'] == PACKAGE_SCHEMA_VERSION:
                    for pkgid, pkgdata in data['packages'].items():
                        for alias in [pkgid] + pkgdata.get('aliases', []):
                            self.all_packages[alias] = pkgdata

        if Paths.LOCAL_PACKAGES_DIR.is_dir():
            import toml
            for filename in Paths.LOCAL_PACKAGES_DIR.rglob('index.toml'):
                try:
                    with filename.open('r') as f:
                        data = toml.load(f)
                        packagetype, package = next(iter(data.items()))
                        assert packagetype == package.get('type', packagetype)
                        package['type'] = package.get('type', packagetype)
                        packageid = package['id']
                        if packageid in self.all_packages:
                            log.warning('Overriding remote package %r with the one in %s!', packageid, filename)
                        self.all_packages[packageid] = package
                except Exception as e:
                    print('In %s...' % (filename,))
                    raise e

    def uninstall(self, packages: bool=False, prefs: bool=False) -> None:
        self._removeModLoader()
        if packages:
            self._removeMods()
        if prefs:
            self._removePrefs()
        self._restore()

    def install(self, force_ml_update: bool=False) -> None:
        msgbox = PostMessagingBox()
        self._checkEnv()
        
        self._downloadLoader(msgbox)
        if self.update_melonloader or force_ml_update:
            self._nukeOldFiles()
        self._backupPristine()
        if self.update_melonloader or force_ml_update:
            self._installDownloadedLoader()
        self._checkLoader()
        self._hashChangedFiles()
        self.syncMods(msgbox)
        self._hashPristine()
        self.cleanupDeadFiles()

    def _checkLoader(self) -> bool:
        failed_files = []
        missing_files = []
        good_files = []
        for fp, hashbytes in ML_HASHES.items():
            fp = Paths.VRC_INSTALL_DIR / fp
            rfp = fp.relative_to(Paths.VRC_INSTALL_DIR)
            if fp.is_file():
                mine = checksum(hashlib.sha512, fp).upper()
                known = binascii.b2a_hex(hashbytes).decode('ascii').upper()
                if mine != known:
                    # with log.error('Melonloader integrity check: FAILED'):
                    #     log.error('File: %s', rfp)
                    #     log.error('Expected SHA512: %s', known)
                    #     log.error('Your SHA512:     %s', mine)
                    failed_files.append(rfp)
                else:
                    good_files.append(rfp)
            else:
                missing_files.append(rfp)
        nMissing = len(missing_files)
        nFailed = len(failed_files)
        nGood = len(good_files)
        success = nMissing + nFailed == 0
        with log.info('Melonloader Integrity Check: %s', '✓' if success else '✗'):
            if nMissing > 0:
                log.warning('Missing files: %d', nMissing)
            if nFailed > 0:
                log.warning('Changed/Corrupt files: %d', nFailed)
            if nGood > 0:
                log.info('Good files: %d', nGood)
        return success

    def _selfUpdate(self) -> None:
        import struct
        import subprocess
        import base64
        import shlex
        import zipfile
        import tarfile
        def encodeStr(inp: str) -> bytes:
            strb: bytes = inp.encode('utf-8')
            return struct.pack('>I', len(strb))+strb
        ep: DownloadEndpoint
        sudata: bytes = b''
        destfile: Path
        tmpdir: Path
        with log.info('Downloading update:'):
            with TemporaryDirectory() as s_tmpdir:
                tmpdir = Path(s_tmpdir)
                if os.name == 'nt':
                    ep = self.main_repo.endpoints[EPlatform.WINDOWS]
                    destfile = tmpdir / f'vrctool-{self.main_repo.version}-win64.zip'
                else:
                    ep = self.main_repo.endpoints[EPlatform.LINUX]
                    destfile = tmpdir / f'vrctool-{self.main_repo.version}-linmost.tar.gz'
                downloadToIfModified(ep.url, str(destfile))
                log.info('Checking hash...')
                if checksum(hashlib.sha512, str(destfile), True) != base64.b64decode(ep.hash):
                    log.critical('Bad download. Manually download and install the update from our gitlab.')
                    destfile.unlink(True)
                    sys.exit(1)
                with log.info('Extracting %s...', destfile):
                    if os.name == 'nt':
                        with zipfile.open(destfile, 'r') as z:
                            z.extractall(tmpdir)
                    else:
                        with tarfile.open(destfile, 'r|gz') as z:
                            z.extractall(tmpdir)
                log.info('Deleting %s...', destfile)
                destfile.unlink(True)

        with log.info('Preparing self-update command packet:'):
            log.info('Version: %d', SELFUPDATE_VERSION)
            sudata += struct.pack('>I', SELFUPDATE_VERSION)
            log.info('VRCTool Process ID: %d', os.getpid())
            sudata += struct.pack('>I', os.getpid())
            log.info('Current Working Directory: %s', os.getcwd())
            sudata += encodeStr(os.getcwd())
            log.info('Command-line args: %r', sys.argv)
            sudata += struct.pack('>I', len(sys.argv))
            for arg in sys.argv[1:]:
                sudata += encodeStr(arg)
            newversion = self.main_repo.version
            log.info('Install Directory: %s', newversion)
            sudata += encodeStr(sys.argv[0])
            args = []
            if '__compiled__' in globals():
                # Nuitka
                if os.name == 'nt':
                    args += [str(tmpdir / 'vrctool.exe')]
                else:
                    args += [str(tmpdir / 'vrctool')]
            else:
                args += [sys.executable, '-m', 'vrctool']
            args += ['self-update', '--config', base64.b64encode(sudata)]
        log.info('Calling %s...', shlex.join(args))
        os.chdir(tmpdir)
        os.execl(args[0], *args[:])

    def restart(self) -> None:
        args = []
        # Nuitka
        if '__compiled__' in globals():
            if os.name == 'nt':
                args = ['vrctool.exe']
            else:
                args = ['vrctool']
        else:
            args = [sys.executable, '-m', 'vrctool']
        args += sys.argv[1:]
        if sys.platform == 'win32':
            args = ['"%s"' % arg for arg in args]
        os.execv(args)

    def _checkEnv(self) -> None:
        with log.info('Checking VRCTool...'):
            log.info('Local version: %s', VERSION)
            log.info('Remote version: %s', self.main_repo.version)
            if self.main_repo.version < VERSION:
                log.error('You are using an older version of VRCTool.')
                if '__compiled__' in globals():
                    self._selfUpdate()
                sys.exit(1)
            if self.main_repo.version > VERSION:
                log.warning('You are using a **newer** version of VRCTool than the repository expects.')
                log.warning('This may cause bugs.')
            if self.main_repo.version == VERSION:
                log.info('You are up to date.')

        with log.info('Checking VRChat...'):
            vrcver = getVRCVersion()
            self.game_version = vrcver
            assert vrcver is not None, 'Could not find VRChat version.'
            with log.info('VRChat Version: %s', vrcver):
                log.info('Major: %d', vrcver.major)
                log.info('Minor: %d', vrcver.minor)
                log.info('Revision: %d', vrcver.revision)
                log.info('Patch: %d', vrcver.patch)
                log.info('Build: %d', vrcver.build)
                log.info('Channel: %r', vrcver.channel)
                assert vrcver.channel == VRChatChannel.RELEASE, 'You must be on the Release version of VRChat to use vrctool.'
                assert vrcver.build >= MIN_ACCEPTABLE_GAME_BUILD, f'You are on a build of VRChat that is too old to use for vrctool.'
                if vrcver.build > CURRENT_GAME_BUILD:
                    log.warning('You are on a version of VRChat that has not been tested against vrctool.')

    def _restore(self) -> None:
        with log.info('Restoring pristine files...'):
            for path in FILES_TOUCHED:
                self.backup(Paths.VRC_INSTALL_DIR / path)

    def _removeMods(self) -> None:
        with log.info('Nuking packages...'):
            self.rmIfExists(Paths.MOD_DIR)
            self.rmIfExists(Paths.PLUGIN_DIR)

    def _removePrefs(self) -> None:
        with log.info('Nuking user data...'):
            self.rmIfExists(Paths.USERDATA_DIR)

    def _hashPristine(self) -> None:
        with log.info('Hashing pristine files...'):
            for path in FILES_TOUCHED:
                self.postPatch(Paths.VRC_INSTALL_DIR / path)

    def _hashChangedFiles(self) -> None:
        with log.info('Hashing patched files...'):
            for path in FILES_TOUCHED:
                self.postPatch(Paths.VRC_INSTALL_DIR / path)

    def _installDownloadedLoader(self) -> None:
        with log.info('Extracting modloader...'):
            with zipfile.ZipFile(MELONLOADER_ZIP.install_path, 'r') as zip_ref:
                zip_ref.extractall(Paths.VRC_INSTALL_DIR)

    def _backupPristine(self) -> None:
        with log.info('Backing up pristine files...'):
            for path in FILES_TOUCHED:
                self.backup(Paths.VRC_INSTALL_DIR / path)

    def _downloadLoader(self, msgbox: PostMessagingBox) -> None:
        self.update_melonloader = False
        with log.info('Downloading MelonLoader...'):
            if not MELONLOADER_ZIP.install_path.parent.is_dir():
                MELONLOADER_ZIP.install_path.parent.mkdir()
                self.update_melonloader = True
            if not MELONLOADER_ZIP.install_path.is_file():
                release = MELONLOADER_ZIP.fetchLatestRelease()
                release.renameFile(Path('UNKNOWN') / 'MelonLoader.x64.zip', Path.cwd() / 'tmp' / 'loader.zip')
                MELONLOADER_ZIP.install(msgbox, release, Paths.VRC_INSTALL_DIR)
                self.update_melonloader = True

            for cdir in (Paths.MOD_DIR, Paths.PLUGIN_DIR, Paths.USERDATA_DIR):
                if not cdir.is_dir():
                    cdir.mkdir()

            res = requests.get(MELONLOADER_SHA512.fetchDownloadURLs()[0])
            res.raise_for_status()
            theirhash = res.text.upper()
            myhash = checksum(hashlib.sha512, MELONLOADER_ZIP.install_path).upper()
            if theirhash != myhash:
                log.warning('File hash mismatch, might be updated. Attempting to update...')
                release = MELONLOADER_ZIP.fetchLatestRelease()
                release.renameFile(Path('UNKNOWN') / 'MelonLoader.x64.zip', Path.cwd() / 'tmp' / 'loader.zip')
                MELONLOADER_ZIP.install(msgbox, release, Paths.VRC_INSTALL_DIR)
                myhash = checksum(hashlib.sha512, MELONLOADER_ZIP.install_path).upper()
                self.update_melonloader = True
                if theirhash != myhash:
                    log.critical('FILE HASH MISMATCH')
                    log.critical('Server: %r', theirhash)
                    log.critical('Local : %r', myhash)
                    return
                else:
                    log.info('Successful!')
            else:
                log.info('Mod loader hash checked out.')

    def _removeModLoader(self) -> None:
        with log.info('Nuking modloader files...'):
            for path in FILES_TO_NUKE_FIRST:
                self.rmIfExists(Paths.VRC_INSTALL_DIR / path)

    def _nukeOldFiles(self) -> None:
        with log.info('Nuking any old modloader files...'):
            for path in FILES_TO_NUKE_FIRST:
                self.rmIfExists(Paths.VRC_INSTALL_DIR / path)

    def cleanupDeadFiles(self) -> None:
        for file in Paths.MOD_DIR.iterdir():
            if not file.is_file():
                continue
            if file not in Package.KEEP:
                log.info(f'rm {file}')
                file.unlink()

    def resolveID(self, pkgid: str) -> Optional[dict]:
        pkgid = pkgid.lower()
        if pkgid not in self.all_packages:
            return None
        return self.all_packages[pkgid]


    def syncMods(self, msgbox: Optional[PostMessagingBox] = None) -> None:
        if msgbox is None:
            msgbox = PostMessagingBox()
        toInstall: Set[str] = set()
        iidx: int = 0
        while True:
            iidx += 1
            conflicts = []
            lenbefore = len(toInstall)
            for packageid in self.enabled_packages:
                with msgbox.forPackage(packageid):
                    packagedata = self.resolveID(packageid)
                    if packagedata is None:
                        msgbox.addWarning(f'{packageid} is missing from the repository.  Skipping.  Consider disabling it.')
                        continue
                    package = None
                    if packagedata['from'] not in Package.PACKAGE_SOURCES:
                        log.error("Unable to install %s: Invalid 'from' directive. Actual: %r, expected: one of %r",
                                    packageid, packagedata['from'], list(Package.PACKAGE_SOURCES.keys()))
                        sys.exit(1)
                    if 'DISABLED' in packagedata.get('flags', []):
                        msgbox.addWarning(f"Unable to install {packageid}: This package is marked DISABLED in the repository. This is usually a temporary measure.")
                        continue
                    package = Package.PACKAGE_SOURCES[packagedata['from']]()
                    package.deserialize(packagedata)
                    toInstall.add(package.id)
                    if len(package.requires) > 0:
                        for pkgid in package.requires:
                            toInstall.add(pkgid)
                    if len(package.conflicts) > 0:
                        cf = []
                        for pkgid in package.conflicts:
                            if pkgid in toInstall:
                                cf += [pkgid]
                        conflicts += [(package.id, sorted(cf))]
            nadded = len(toInstall) - lenbefore
            nconflicts = len(conflicts)
            log.info('Precalc #%d: %d added, %d conflicts', iidx, nadded, nconflicts)
            if nconflicts > 0:
                with log.error('Conflicts:'):
                    for pkgid, pkgconflicts in conflicts:
                        log.error('%s: %s', pkgid, ', '.join(pkgconflicts))
                sys.exit(1)
            if nadded > 0:
                continue
            break
        for pkgid in toInstall:
            with log.info('Installing %s...', pkgid):
                with msgbox.forPackage(pkgid):
                    packagedata = self.resolveID(pkgid)
                    package = Package.PACKAGE_SOURCES[packagedata['from']]()
                    package.id = pkgid
                    package.deserialize(packagedata)
                    if self.game_version is None:
                        self.game_version = getVRCVersion()
                    package._do_install(msgbox, self.game_version.build)

        msgbox.toLog(log)

    def backup(self, filename: Path):
        lockfile = Path(filename)
        lockfile.suffixes.append('.lock')
        backupfile = Path(filename)
        backupfile.suffixes.append('.bak')
        if lockfile.is_file() and backupfile.is_file():
            data = {}
            with open(lockfile, 'r') as f:
                data = json.load(f)
            targetmd5 = data['t']
            backupmd5 = data['b']
            if (targetmd5 is None or checksum(hashlib.md5, filename) == targetmd5) and checksum(hashlib.md5, backupfile) == backupmd5:
                log.info('Backup not needed. (Hashes match)')
                return
        for f in (backupfile, lockfile):
            self.rmIfExists(f)
        if not backupfile.parent.is_dir():
            backupfile.parent.mkdir(parents=True)
        shutil.copy(filename, backupfile)

    def postPatch(self, filename: Path):
        lockfile = Path(filename)
        lockfile.suffixes.append('.lock')
        backupfile = Path(filename)
        backupfile.suffixes.append('.bak')
        log.info('Writing %s...', lockfile)
        with open(lockfile, 'w') as f:
            data = {
                't': checksum(hashlib.md5, filename),
                'b': checksum(hashlib.md5, backupfile)
            }
            json.dump(data, f, indent=None)

    def rmIfExists(self, filename: Path) -> None:
        if os.path.isfile(filename):
            log.info('rm %s', filename)
            os.remove(filename)
        elif os.path.isdir(filename):
            log.info('rm -r %s', filename)
            shutil.rmtree(filename)

    def restore(self, filename: Path):
        lockfile = Path(filename)
        lockfile.suffixes.append('.lock')
        backupfile = Path(filename)
        backupfile.suffixes.append('.bak')
        if not lockfile.is_file():
            log.error('Cannot restore %s - LOCK file missing. You will have to revalidate.', filename)
            return False
        if not backupfile.is_file():
            log.error('Cannot restore %s - BAK file missing. You will have to revalidate.', backupfile)
            return False

        data = {}
        with open(lockfile, 'r') as f:
            data = json.load(f)
        targetmd5 = data['t']
        backupmd5 = data['b']
        if checksum(hashlib.md5, filename) != targetmd5 or checksum(hashlib.md5, backupfile) != backupmd5:
            log.error('Cannot restore %s - Hash mismatch. You will have to revalidate.', filename)
            return False
        self.rmIfExists(filename)
        if not filename.parent.is_dir():
            filename.parent.mkdir(parents=True)
        shutil.copy(backupfile, filename)
        for f in (backupfile, lockfile):
            self.rmIfExists(f)
        return True

    def save(self) -> None:
        data = {
            'version': CONFIG_VERSION,
            'paths': Paths.Serialize(),
            'packages': self.enabled_packages,
            'loader': {
                'zip': MELONLOADER_ZIP.serialize(), # v0.3.0
                'sha512': MELONLOADER_SHA512.serialize(), # v0.3.0
            },
        }
        newconfig = Paths.CONFIG_FILE.with_suffix('.new')
        with newconfig.open('w') as f:
            json.dump(data, f, indent=2)
        os.replace(newconfig, Paths.CONFIG_FILE)
