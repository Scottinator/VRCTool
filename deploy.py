import argparse
import hashlib
import os
import sys
from pathlib import Path

import gitlab
import toml
from buildtools import http, os_utils, utils
from mega import Mega
from pygit2 import Commit, Reference, Repository
from ruamel.yaml import YAML
#from jenkins import Jenkins
from semantic_version import Version

PLATFORMS = [
    ('linux', 'tar.gz'),
    ('windows', 'zip'),
]

POSIX_DEFAULT_CFG = Path('/etc/python-gitlab.cfg')
NT_DEFAULT_CFG = Path(os.environ['HOME']) / 'python-gitlab.cfg'
DEFAULT_CFG = str(NT_DEFAULT_CFG if os.name == 'nt' else POSIX_DEFAULT_CFG)

yaml = YAML(typ='safe', pure=True)

with Path('.deploy.yml').open('r') as f:
    cfg = yaml.load(f)

argp = argparse.ArgumentParser()
argp.add_argument('version', type=str, help='Tag of the release')
argp.add_argument('subtitle', type=str, help='Subtitle of the release')
args = argp.parse_args()

with open('RELEASE_NOTES.md', 'r') as f:
    RELEASE_NOTES = f.read()

JENKINS_SCHEME: str = cfg["jenkins"]["scheme"]
JENKINS_HOST: str = cfg["jenkins"]["hostname"]
JENKINS_PORT: int = int(cfg["jenkins"]["port"])
JENKINS_USERNAME: str = cfg["jenkins"]["username"]
JENKINS_PASSWORD: str = cfg["jenkins"]["password"]
JENKINS_JOB: str = cfg["jenkins"]["job"]
JENKINS_BASE: str = f'{JENKINS_SCHEME}://{JENKINS_HOST}:{JENKINS_PORT}'

USE_MEGA = False
MEGA_EMAIL = cfg['mega']['email']
MEGA_PASSWORD = cfg['mega']['password']

USE_SCP = True
SCP_HOST = cfg['scp']['hostname']
SCP_USER = cfg['scp']['username']
SCP_PATH = cfg['scp']['path']
SCP_URI = cfg['scp']['uri']

GITLAB_ID = cfg['gitlab']['id']
GITLAB_PROJECT_ID = int(cfg['gitlab']['project'])

VRCTOOL_PACKAGES_DIR = Path(cfg['paths']['packages'])
VRCTOOL_PACKAGES_REPO_TOML = VRCTOOL_PACKAGES_DIR / 'repo.toml' 

def sha512sum(path: os.PathLike) -> str:
    with open(path, 'rb') as f:
        return utils.hashfile(f, hashlib.sha512())

os_utils.ensureDirExists(os.path.join('tmp', 'dist'))

print('Performing idiot checks:')
def success():
    print('YES'.rjust(30))
def failure():
    print('NO'.rjust(30))
    sys.exit(1)

print(f'  String {args.version!r} can be parsed as semver?')
try:
    Version(args.version)
    success()
except:
    failure()

print(f'  Tag v{args.version} exists?')
commit: Commit
ref: Reference
try:
    repo = Repository('.')
    commit, ref = repo.resolve_refish(f'v{args.version}')
    print('    Commit', commit.id)
    print('    Ref', str(ref.target))
    success()
except Exception as e:
    print(e)
    failure()

print(f'  File {VRCTOOL_PACKAGES_REPO_TOML} exists?')
if VRCTOOL_PACKAGES_REPO_TOML.is_file():
    success()
else:
    failure()

if USE_MEGA:
    mega = Mega()
    print('Logging into mega.nz...')
    mega.login(MEGA_EMAIL, MEGA_PASSWORD)

#print('Logging into jenkins...')
#jenkins = Jenkins(JENKINS_BASE, username=JENKINS_USERNAME, password=JENKINS_PASSWORD)
if USE_MEGA:
    MEGA_VRCTOOL_FOLDER = mega.find('vrctool')
files = []
endpoints = {}
# http://xxxxx:8080/job/vrctool-release/lastSuccessfulBuild/artifact/archives/vrctool-linux-amd64-0.2.2.tar.gz
for platform, ext in PLATFORMS:
    destfile = f'tmp/dist/vrctool-{platform}-amd64-{args.version}.{ext}'
    http.DownloadFile(f'{JENKINS_BASE}/job/{JENKINS_JOB}/lastSuccessfulBuild/artifact/archives/vrctool-{platform}-amd64-{args.version}.{ext}',
                      destfile, auth=(JENKINS_USERNAME, JENKINS_PASSWORD))
    sz = os.path.getsize(destfile)
    _hash = sha512sum(destfile)
    link: str = ''
    if USE_MEGA:
        md = mega.find(f'vrctool/vrctool-{platform}-amd64-{args.version}.{ext}')
        if md:
            print('Deleting duplicate...')
            mega.delete(md[0])
        print(f'Uploading {destfile} to vrctool...')
        md = mega.upload(destfile, dest=MEGA_VRCTOOL_FOLDER[0])
        link = mega.get_upload_link(md)
    elif USE_SCP:
        os_utils.cmd(['scp', str(destfile), f'{SCP_USER}@{SCP_HOST}:{SCP_PATH}'])
        link = f'{SCP_URI}/vrctool-{platform}-amd64-{args.version}.{ext}'
    files += [[f'vrctool-{platform}-amd64-{args.version}.{ext}',
               sz, _hash, link]]
    endpoints[platform.upper()] = {
        'url': link,
        'hash': _hash
    }

RELEASE_NOTES += '\n## Binary Builds\n'
RELEASE_NOTES += '\nUse these if you\'re a Windows user or someone who can\'t work Python.\n'
RELEASE_NOTES += '| Filename | Size | SHA512 |\n'
RELEASE_NOTES += '|---:|---|---|\n'
links = []
for filename, sz, _hash, link in files:
    humansz = os_utils.sizeof_fmt(sz)
    RELEASE_NOTES += f'| [{filename}]({link}) | {humansz} ({sz}) | <code>{_hash}</code> |\n'
    links += [{
        'name': filename,
        'url': link,
        'type': 'other'
    }]

gl = gitlab.Gitlab.from_config(GITLAB_ID, [DEFAULT_CFG])
print(f'Logging into gitlab...')
gl.auth()

print(f'Fetching project...')
project = gl.projects.get(GITLAB_PROJECT_ID)
print(f'Creating release...')
project.releases.create({
    'name': f'v{args.version} - {args.subtitle}',
    'ref': str(ref.target),
    'tag_name': f'v{args.version}',
    'description': RELEASE_NOTES,
    'assets': {
        'links': links
    }
})
print('Updating vrctool-packages...')
data = {}
with open(VRCTOOL_PACKAGES_REPO_TOML, 'r') as f:
    data = toml.load(f)
data['repo']['vrctool'] = {
    'version': args.version,
    'endpoints': endpoints,
}
with open(VRCTOOL_PACKAGES_REPO_TOML, 'w') as f:
    toml.dump(data, f)

with os_utils.Chdir(str(VRCTOOL_PACKAGES_DIR)):
    os_utils.cmd(['.venv/bin/python', 'devtools/build.py'], echo=True, show_output=True)
print(f'DONE')
