# VRCTool
*A Swiss-army knife for VRChat&reg;.*

This tool installs and maintains MelonLoader and its mods.  In addition, it includes some common troubleshooting tools.

[[_TOC_]]

## Disclaimers

Please read this, particularly the sections regarding VRChat's Terms of Service and MelonLoader.

All of these legal disclaimers are necessary due to VRChat Inc's history of threats.

### Regarding VRChat&reg; Terms of Service

**Using this tool to mod VRChat&reg; can result in a ban from VRChat.**  VRChat&reg; does not allow *any* modifications to the client, without exception. While VRC has been mumbling that they may be changing their minds on the subject (Apr 1 2021), so far they have not made any concrete steps to change their terms of service. **Always assume you are being watched.**

We do not take responsibility for your actions, nor the consequences of your actions. All we can ask is that you use VRCTool responsibly and not be an asshole.

**Use VRCTool at your own risk.**

### Regarding MelonLoader

This tool installs MelonLoader for you during the process of setting up your mods.

**Using this tool to install Melonloader is against MelonLoader terms of use.** Any bugs should be reported to us rather than Melonloader. We are not affiliated or even condoned by MelonLoader's developers. **If you run into problems, run `vrctool uninstall` and re-install Melonloader through their installer.**

While we try to install things as close to the original installer as possible, we can be behind the curve, and we can't call their installer for a number of reasons.  Please file a bug if we fuck something up.

### Trademarks and Affiliations

VRCTool is **not** owned by, associated with, nor condoned by VRChat Inc., VRChat Inc. staff, nor MelonLoader or MelonLoader staff.

VRChat&reg; is a registered trademark of VRChat Incorporated. VRCTool is neither affiliated with nor approved by VRChat Incorporated.

All third-party trademarks (including logos and icons) referenced by VRCTool remain the property of their respective owners.  Unless specifically identified as such, VRCTools' use of third-party trademarks does not indicate any relationship, sponsorship, or endorsement between VRCTool and the owners of these trademarks.  Any references by VRCTool to third-party trademarks is to identify corresponding party third-party goods and/or services and shall be considered nominative fair use under US trademark law.

### Copyrights

While we put good effort into avoiding it, VRCTool may contain copyrighted material, the use of which may not have been specifically authorized by the copyright owner.  This material is used under fair use as established in *Google LLC v. Oracle America, Inc*:  This code is used to maintain interoperability with copyrighted software (specifically cache management and user settings in VRChat), and is relatively small and differently implemented, and could not be used to easily duplicate the work.  The material is used and distributed without profit.

This should constitute a fair use of any such copyrighted material (referenced and provided for in section 107 of the US Copyright Law).

## Installing

### Windows

1. Download and extract vrctool.7z to a folder somewhere.
1. Type `WINKEY + R` to open `Run`.
1. Enter `cmd` into the prompt and press enter.
1. `cd` to the folder. ex: `cd /d C:\vrctool`
1. Run `vrctool --help` to receive the help text.

### Linux

We do not currently support VRChat on Linux.

### Mac OSX

Fuck off no one likes you

## Installing MelonLoader

```shell
$ vrctool install
```

This will download and install MelonLoader with the mods you have installed.  At this stage, you have no mods installed, yet.

## Installing mods

```shell
$ vrctool enable ActionMenuUtils AdvancedSafety AskToPortal ClickFix ComponentToggle DownloadFix emmVRC TrueShaderAntiCrash UIExpansionKit
```

This command can download and install several mods at once. It is not case-sensitive.

## Removing mods
```shell
$ vrctool disable emmVRC
```
This command can remove multiple mods at once.  It is not case-sensitive.

## Cache Operations

### Reading size of cache

```shell
$ vrctool cache stats
```
```
%LOCALAPPDATA%Low/VRChat/vrchat/Cache-WindowsPlayer:
  File Count: 1653
  Total Size: 18.2GiB
  Average Size: 11.2MiB
%LOCALAPPDATA%Low/VRChat/vrchat/HTTPCache-WindowsPlayer:
  File Count: 0
  Total Size: 0.0B
%LOCALAPPDATA%Low/VRChat/vrchat/Unity:
  File Count: 2
  Total Size: 500.0B
  Average Size: 250.0B
%LOCALAPPDATA%Low/VRChat/vrchat/Cookies:
  File Count: 1
  Total Size: 75.0B
  Average Size: 75.0B
Total:
  File Count: 1656
  Total Size: 18.2GiB
  Average Size: 11.2MiB
```

This just walks over your cache directory's contents and returns the total file size.

### Clearing cache

```shell
$ vrctool cache clear
```
### Cleaning up cache

This currently deletes a bunch of empty folders that Unity leaves behind when it removes an item from cache.

```shell
$ vrctool cache sweep
```

## Adding Mods (On your machine only)

1. Create a folder called `packages.local` in your install directory.
1. Create another folder inside of `packages.local` which is the name of your mod.
1. Create a file inside of your mod folder called `index.toml` with the following content:
```toml
# Use [mod] or [plugin]. Affects install path.
[mod]
# Unique ID of the mod, in lower-case.
id = "togglepostprocessing"

# Use only ONE of the following blocks:
####
# github:
from = "github"
#  Group/User name. (First part of the URL after github.com)
namespace = "Scottinator"
# Project name slug (second part of the URL after github.com)
project = "ProjectName"
# Filename to look for in Releases.
filename = "TogglePostProcessing.dll"
#allow_prerelease = true

####
# OR gitlab:
from = "gitlab"
# Hostname to interface with (such as gitlab.com or gitgud.io)
hostname = "my.gitlab.tld"
# Group/User name. (First part of the URL after hostname)
namespace = "Scottinator"
# Project name slug (second part of the URL after hostname)
project = "ProjectName"
# Filename to look for in Releases.
filename = "TogglePostProcessing.dll"
#allow_prerelease = true

####
# OR Direct HTTP link:
from = "http"
# URL to send a GET request to.
url = "https://www.thetrueyoshifan.com/downloads/emmVRCLoader.dll"

####
# OR local file on disk:
from = "file"
source-file = "C:\\Path\\To\\File.dll"
```
