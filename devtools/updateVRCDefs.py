import hashlib
import shutil
from buildtools import utils
from typing import List
from buildtools import os_utils, log, http
import buildtools
from buildtools.cli import getInputLine
from pathlib import Path
import json
import random
import string
import sys
import tqdm

VRCUPDATER_CFG = Path().cwd() / '.vrcupd.json'
VRCHAT_DIR = Path().cwd() / '.vrcsample'

CHAR_TABLES = [
    string.ascii_lowercase, # 26
    string.ascii_uppercase, # 26
    string.digits, # 10
    string.punctuation+' ',
]

TIDX_WIDTH = max([i.bit_length() for i in range(len(CHAR_TABLES))])
TIDX_MASK = (1 << TIDX_WIDTH) - 1 #sum([(1 << i) for i in range(0, TIDX_WIDTH)])
#print('T', TIDX_WIDTH, bin(TIDX_MASK))
CIDX_WIDTH = max([len(t).bit_length() for t in CHAR_TABLES])
CIDX_MASK = ((1 << CIDX_WIDTH) - 1) << TIDX_WIDTH #sum([(1 << i) for i in range(TIDX_WIDTH, TIDX_WIDTH+CIDX_WIDTH)])
#print('C', CIDX_WIDTH, bin(CIDX_MASK))

def encodeStr(input: str) -> List[int]:
    o=[]
    for c in input:
        for ti in range(len(CHAR_TABLES)):
            if (ci := CHAR_TABLES[ti].find(c)) > -1:
                p = random.getrandbits(32)
                #print('E', c, ti, ci)
                #print(bin(p))
                p &= ~(TIDX_MASK | CIDX_MASK)
                #print(bin(p))
                p |= (ci << TIDX_WIDTH) | ti
                #print(bin(p))
                o.append(p)
    return o
def decodeStr(input: List[int]) -> str:
    o=''
    for n in input:
        ci = (n & CIDX_MASK) >> TIDX_WIDTH
        ti = (n & TIDX_MASK)
        c = CHAR_TABLES[ti][ci]
        #print('D', ti, ci, c)
        o += c
    return o
assert decodeStr(encodeStr('boB.123'))=='boB.123'
def main() -> int:
    import argparse
    argp = argparse.ArgumentParser('updateVRCDefs.py')
    argp.add_argument('--reset-config', action='store_true', default=False)
    
    args = argp.parse_args()
    if args.reset_config or not VRCUPDATER_CFG.is_file():
        log.warning('Configuration does not exist.')
        log.info('Steam Username:')
        cfg = {}
        cfg['u'] = getInputLine('Steam Username')
        p = getInputLine('Steam Password')
        cfg['p'] = encodeStr(p)
        
        with VRCUPDATER_CFG.open('w') as f:
            json.dump(cfg, f, separators=(',',':'))
        if os_utils.is_linux():
            VRCUPDATER_CFG.chmod(0o600)
    fmode = VRCUPDATER_CFG.stat().st_mode if os_utils.is_linux() else -1
    if fmode in (0, 0o600):
        log.error(f'The mode of {VRCUPDATER_CFG} is {fmode:o}, not 0o600! This is a HUGE security risk.')
        log.error(f'FIX: $ chmod 600 {VRCUPDATER_CFG}')
        return 1
    data: dict
    with VRCUPDATER_CFG.open('r') as f:
        data = json.load(f)
    username = data['u']
    password: str = decodeStr(data['p'])

    cmd = [os_utils.which('steamcmd'), 
        '+@sSteamCmdForcePlatformType', 'windows',
        '+force_install_dir', str(VRCHAT_DIR.absolute()),
        '+login', username, password,
        '+app_update', '438100', 'validate',
        '+quit'
    ]
    os_utils.cmd(cmd, show_output=True)
    with open('vrcversiondata.json', 'w') as f:
        env = os_utils.ENV.clone()
        env.set('VRCHAT_DIR', str(VRCHAT_DIR.absolute()))
        f.write(os_utils.cmd_out([sys.executable, '-m', 'vrctool', 'show', 'vrc', '--for-build'], env=env))
    with open('vrcmanifest.json', 'w') as f:
        o={}
        files = []
        for filename in VRCHAT_DIR.rglob('*'):
            if filename.is_file() and filename.suffix not in ('.acf', '.vrca', '.vrcw'):
                files.append(filename)
        for filename in tqdm.tqdm(files, unit='f'):
            o[str(filename.relative_to(VRCHAT_DIR))] = utils.hashfile(str(filename), hashlib.sha512())
        json.dump(o, f, indent=2, sort_keys=True)#separators=(',',':'))
    print(os_utils.cmd_out([sys.executable, '-m', 'vrctool', 'show', 'vrc'], env=env))
    return 0

if __name__ == '__main__':
    main()