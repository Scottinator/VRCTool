#!/bin/bash
set -ex
PYTHON="$PYTHON"
[ -z "$PYTHON" ] && [ ! -z "${VIRTUAL_ENV}" ] && { PYTHON=`command -v python`; }
[ -z "$PYTHON" ] && command -v python3.10 && { PYTHON=`command -v python3.10`; }
[ -z "$PYTHON" ] && command -v python3.9 && { PYTHON=`command -v python3.9`; }
[ -z "$PYTHON" ] && command -v python3.8 && { PYTHON=`command -v python3.8`; }
[ -z "$PYTHON" ] && command -v python3.7 && { PYTHON=`command -v python3.7`; }
[ -z "$PYTHON" ] && command -v python3.6 && { PYTHON=`command -v python3.6`; }
[ -z "$PYTHON" ] && command -v python3 && { PYTHON=`command -v python3`; echo "WARNING: Using fallback interpreter!"; }
if [ -z "$PYTHON" ]; then
  echo "Unable to find an available python3 interpreter. Please install python >=3.6."
  exit 1
fi
echo \$PYTHON=$PYTHON

VERSION="$VERSION"
[ -z "$VERSION" ] && { VERSION="ci"; }

vnm i --dev
. .venv/bin/activate
.venv/bin/python3 devtools/build.py --no-colors --rebuild --version=$VERSION --deploy-to=/media/builds/vrctool
