@echo off
:: Windows has shitty naming schemes for Python, so you have to suffer.
SET PYTHON=%PYTHON%
if [%PYTHON%]==[] (
  for %%x in (.venv\Scripts\python.exe, python3.8, python38, python3.7, python37, python3.6, python36, python3) do (
    WHERE %%x
    if %ERRORLEVEL%==0 (
      set PYTHON=%%x
      goto eggsit
    )
  )
)

:eggsit
if [%PYTHON%]==[] (
  echo ERROR: The command %PYTHON% is missing from PATH. Please install Python from http://python.org and add it to PATH.
  exit 1
)
echo PYTHON=%PYTHON%

if [%VERSION%]==[] (
  set VERSION=ci
)
echo VERSION=%VERSION%
@echo on

vnm i --dev
.venv\Scripts\python.exe devtools\build.py --no-colors --rebuild --version=%VERSION% --deploy-to=\\192.168.9.5\host\builds\vrctool
