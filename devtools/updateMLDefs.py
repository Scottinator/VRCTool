import hashlib
import re
import shutil
from buildtools import utils
from typing import Dict, List
from buildtools import os_utils, log, http
import buildtools
from buildtools.cli import getInputLine
from pathlib import Path
import json
import random
import string
import sys
import tqdm
import requests

ML_VERSION    = 'v0.4.3'
UNITY_VERSION = '2019.4.30'
VRC_VERSION   = 1132

VRCUPDATER_CFG = Path().cwd() / '.vrcupd.json'
VRCHAT_DIR = Path().cwd() / '.vrcsample'
TMP_DIR = Path().cwd() / '.tmp'
COMBINED_DIR = TMP_DIR / '.combined'
ML_BASE_PATH = TMP_DIR / 'melonloader'

LATEST_CPP2IL_URI = 'https://raw.githubusercontent.com/LavaGang/MelonLoader/master/Il2CppAssemblyGenerator/Packages/Cpp2IL.cs'
LATEST_ASMDUMP_URI = 'https://raw.githubusercontent.com/LavaGang/MelonLoader/master/Il2CppAssemblyGenerator/Packages/Il2CppAssemblyUnhollower.cs'
ML_API_URI = 'https://api.melonloader.com/api/v1/game/vrchat'

CPP2IL = Path('bin') / 'cpp2il'
CPP2IL_OUT = TMP_DIR / 'cpp2il_out'

ASMDUMP = ML_BASE_PATH / 'cpp2il'
CPP2IL_OUT = TMP_DIR / 'cpp2il_out'

REG_ENV_LINE = re.compile(r'\{"([^"]+)", ?"([^"]+)"\},?')
# parameters.Add($"--input={ Core.dumper.Output }");
REG_PARAM_ADD = re.compile(r'parameters\.Add\(\$?"(.+)");')
def argsFromCS(text: str) -> List[str]:
    args: List[str] = []
    env: Dict[str: str] = {}
    line: str
    in_args=False
    in_env=False
    for line in text.splitlines(False):
        sline = line.strip()
        if sline == 'return Execute(new string[] {':
            in_args = True
            in_env = False
            continue
        if sline == '}, false, new Dictionary<string, string>() {':
            in_args = False
            in_env = True
            continue
        if sline == '});':
            in_args = False
            in_env = False
            continue
        if sline.startswith('parameters.Add('):
            if m := REG_PARAM_ADD.match(sline):
                args.append(m[1])
        if in_args:
            if sline.startswith('"') and not sline.startswith('"\""'):
                arg = sline[1:sline.index('"', 1)]
                args.append(arg)
        if in_env:
            if m := REG_ENV_LINE.match(sline):
                env[m[1]] = m[2]
def main() -> int:
    global VRC_VERSION
    with open('vrcversiondata.json', 'r') as f:
        VRC_VERSION = json.load(f)['build']

    req = requests.get(ML_API_URI)
    req.raise_for_status()
    data = req.json()
    deobf_map = data['mappingUrl']
    deobf_hash = data['mappingFileSHA512']

    req = requests.get(LATEST_CPP2IL_URI)
    req.raise_for_status()
    cpp2il_cs = req.text

    CPP2IL_VERSION = data.get('forceCpp2IlVersion')
    CPP2IL_ARGS = [CPP2IL]
    for arg in argsFromCS(cpp2il_cs):
        CPP2IL_ARGS.append(arg)
        if arg == '--game-path':
            CPP2IL_ARGS.append(str(COMBINED_DIR / 'GameAssembly.dll'))

    if not CPP2IL_VERSION:
        if m := re.search(r'\bVersion = "(\d{4}\.\d+\.\d+)";', cpp2il_cs):
            CPP2IL_VERSION = m[1]

    req = requests.get(LATEST_ASMDUMP_URI)
    req.raise_for_status()
    asmdump_cs = req.text
    ASMDUMP_ARGS = [ASMDUMP]
    '''
    parameters.Add($"--input={ Core.dumper.Output }");
    parameters.Add($"--output={ Output }");
    parameters.Add($"--mscorlib={ Path.Combine(Core.ManagedPath, "mscorlib.dll") }");
    parameters.Add($"--unity={ Core.unitydependencies.Destination }");
    parameters.Add($"--gameassembly={ Core.GameAssemblyPath }");
    '''
    for arg in argsFromCS(asmdump_cs):
        arg, value = arg.split('=',1)
        if arg == '--input':
            value = str(CPP2IL_OUT)
        if arg == '--output':
            value = str(ASMDUMP_OUT)
        if arg == '--mscorlib':
            value = str(DEPS_DIR / 'Managed' / 'mscorlib.dll')
        if arg == '--unity':
            value = str(DEPS_DIR / 'Managed')
        if arg == '--gameassembly':
            value = str(COMBINED_DIR / 'GameAssembly.dll')
        ASMDUMP_ARGS.add(f'{arg}={value}')
    

    MELONLOADER_DIR = Path('tmp') / 'melonloader_install'
    MELONLOADER_ZIP = Path('tmp') / 'melonloader.zip'
    MELONLOADER_ZIP.unlink(missing_ok=True)
    if MELONLOADER_DIR.is_dir():
        shutil.rmtree(MELONLOADER_DIR)
    http.DownloadFile(f'https://github.com/LavaGang/MelonLoader/releases/download/{ML_VERSION}/MelonLoader.x64.zip', str(MELONLOADER_ZIP))
    os_utils.decompressFile(str(MELONLOADER_ZIP), str(MELONLOADER_DIR))

    DEPS_DIR = Path('tmp') / 'deps'
    DEPS_ZIP = Path('tmp') / 'deps.zip'
    DEPS_ZIP.unlink(missing_ok=True)
    if DEPS_DIR.is_dir():
        shutil.rmtree(DEPS_DIR)
    http.DownloadFile(f'https://github.com/LavaGang/Unity-Runtime-Libraries/raw/master/{UNITY_VERSION}.zip', str(DEPS_ZIP))
    os_utils.decompressFile(str(MELONLOADER_ZIP), str(DEPS_DIR))

    CPP2IL.unlink(missing_ok=True)
    DUMPER_VERSION = '2021.2.4'
    http.DownloadFile(f'https://github.com/SamboyCoding/Cpp2IL/releases/download/{CPP2IL_VERSION}/Cpp2IL-FORCED_DUMPER-Linux', str(CPP2IL))
    

    with open('mlmanifest.json', 'w') as f:
        o={}
        files = []
        for filename in MELONLOADER_DIR.rglob('*'):
            if filename.is_file() and filename.suffix not in ('.acf', ):
                files.append(filename)
        for filename in tqdm.tqdm(files, unit='f'):
            o[str(filename.relative_to(MELONLOADER_DIR))] = utils.hashfile(str(filename), hashlib.sha512())
        json.dump(o, f, indent=2, sort_keys=True)#separators=(',',':'))
    return 0

if __name__ == '__main__':
    main()