import json
import toml
import os
import re
import shutil
import binascii
import base64
import jinja2
import lzma
import tarfile, zipfile
from pathlib import Path

from typing import List

from buildtools import log, os_utils
from buildtools.maestro import BuildMaestro
from buildtools.maestro.base_target import SingleBuildTarget
from buildtools.maestro.fileio import CopyFileTarget, CopyFilesTarget, MoveFileTarget, ReplaceTextTarget
from buildtools.maestro.shell import CommandBuildTarget
from buildtools.maestro.nuitka import NuitkaTarget

USE_BASE64 = True

(shutil.rmtree(x) for x in ('dist', 'build'))

SANDBOX_DIR: Path
env = os_utils.ENV.clone()
if os_utils.is_windows():
    env.prependTo('PATH', str(Path(__file__).parent / '.venv' / 'Scripts'))
    SANDBOX_DIR = Path(env.get('TEMP')) / 'vrctool-build'
if os_utils.is_linux():
    env.prependTo('PATH', str(Path(__file__).parent / '.venv' / 'bin'))
    SANDBOX_DIR = Path('/tmp') / 'vrctool-build'

DOC_SRC_DIR: Path = Path.cwd() / 'src' / 'docs'
NUITKA_PKG_NAME: str = '__main__'
NUITKA_ENTRY_POINT: Path = Path('vrctool') / '__main__.py'
NUITKA_OUT_DIR: Path = Path('tmp') / 'nuitka'
NUITKA_DIST_DIR: Path = NUITKA_OUT_DIR / f'{NUITKA_PKG_NAME}.dist'
NUITKA_EXECUTABLE: Path
DIST_DIR = Path('dist')
DIST_EXECUTABLE: Path
DIST_EXECUTABLE_MANGLED: Path
SRC_DOCS_DIR = Path('src') / 'docs'
DOCS_DIR = Path('docs')
DIST_DIR = Path('dist')

'''def encode_key(data: bytes, prefix='', linelen=1000) -> str:
    d = lzma.compress(data)
    n = 30
    d = [int(i) for i in d]
    start = '' #"gzip.decompress("
    end = '' # ')'
    plen = len(prefix)+len(start)
    p2 = ' '*plen
    left = len(d)
    o = []
    while left > 0:
        cd = d[:n]
        d = d[n:]
        cb = ''.join([f'\\\\x{x:02X}' for x in cd])
        o += [f"b'{cb}'"]
        left -= n
    o = f' + \\\n{p2}'.join(o)
    return f"{start}{o}{end}"
'''

def encode_key(data: bytes, prefix='', linelen=200) -> str:
    if not USE_BASE64:
        return repr(data)
    else:
        d = data #lzma.compress(data)
        d = base64.b64encode(d).decode('ascii')
        #n=64
        start = 'b64d(' #"gzip.decompress("
        end = ')'
        plen = len(prefix)+len(start)
        p2 = ' '*plen
        left = len(d)
        o = []
        while left > 0:
            cd = d[:linelen]
            d = d[linelen:]
            o += [f"'{cd}'"]
            left -= linelen
        o = f' + \\\n{p2}'.join(o)
        return f"{start}{o}{end}"
        

def buildPath(s: str) -> Path:
    chunks = s.split('/')
    o = [f'Path({chunks[0]!r})']
    if len(chunks) > 1:
        o += [repr(c) for c in chunks[1:]]
    return ' / '.join(o)

def getVRCHashes() -> str:
    with open('vrcmanifest.json', 'r') as f:
        mf = json.load(f)
    o = '{\n'
    for k,v in mf.items():
        v=binascii.a2b_hex(v)
        if USE_BASE64:
            o += f'    ({buildPath(k)}): {encode_key(v, linelen=1000)},\n'
        else:
            o += f'    ({buildPath(k)}): {re.escape(encode_key(v, linelen=1000))},\n'
    return o+'}\n'
def getMLHashes() -> str:
    with open('mlmanifest.json', 'r') as f:
        mf = json.load(f)
    o = '{\n'
    for k,v in mf.items():
        v=binascii.a2b_hex(v)
        if USE_BASE64:
            o += f'    ({buildPath(k)}): {encode_key(v, linelen=1000)},\n'
        else:
            o += f'    ({buildPath(k)}): {re.escape(encode_key(v, linelen=1000))},\n'
    return o+'}\n'

class MakeTarGZ(SingleBuildTarget):
    BT_TYPE = 'TAR.GZ'

    def __init__(self, target, files=[], start='.', dependencies=[]):
        self.start = '.'
        super().__init__(target, files, dependencies=dependencies)

    def build(self):
        with tarfile.open(self.target, "w:gz") as tar:
            for af in self.files:
                tar.add(af, arcname=os.path.relpath(af, start=self.start))


class MakeZip(SingleBuildTarget):
    BT_TYPE = 'ZIP'

    def __init__(self, target, files=[], start='.', dependencies=[]):
        self.start = '.'
        super().__init__(target, files, dependencies=dependencies)

    def build(self):
        with zipfile.ZipFile(self.target, "w", compression=zipfile.ZIP_DEFLATED) as z:
            flist: List[str] = []
            for af in self.files:
                if os.path.isfile(af):
                    flist += [af]
                elif os.path.isdir(af):
                    for root, _, files in os.walk(af):
                        for filename in files:
                            flist += [os.path.join(root, filename)]
            for af in flist:
                z.write(af, arcname=os.path.relpath(af, start=self.start))

class GenDocTarget(SingleBuildTarget):
    BT_TYPE = 'GEN DOC'
    GLOBAL_DOCVARS: dict = None

    def __init__(self, srcdir, destdir, basedir, basename, dependencies=[], header: bool = True, strip_comments: bool = False):
        self.outfile = os.path.join(destdir, basedir, f'{basename}.md')
        self.infile = os.path.join(srcdir, basedir, f'{basename}.tmpl.md')
        self.header = header
        self.vars = {}
        self.strip_comments: bool = strip_comments

        super().__init__(self.outfile, [self.infile])

    def build(self):
        global _jenv, env

        if self.GLOBAL_DOCVARS is None:
            self.GLOBAL_DOCVARS = {}
            with open('vrctool/consts.py.in', 'r') as f:
                m = re.search(r"VERSION = Version\('([^']+)'", f.read())
                assert m is not None
                print(repr(m.groups()))
                self.GLOBAL_DOCVARS['VERSION'] = m[1]
            #print(repr(self.GLOBAL_DOCVARS))

        _jenv = jinja2.Environment(
            loader=jinja2.FileSystemLoader(DOC_SRC_DIR),
            extensions=['jinja2.ext.do'],
            autoescape=False
        )

        _jenv.globals.update(**self.GLOBAL_DOCVARS)

        if env.get('HOME', '') != SANDBOX_DIR:
           env.set('HOME', SANDBOX_DIR)  # hue

        rendered = ''
        if self.header:
            rendered += '<!--\n'
            rendered += f'@GENERATED by {os.path.relpath(__file__)}. DO NOT MANUALLY EDIT.\n'
            rendered += f'Edit {os.path.normpath(self.infile)} instead!\n'
            rendered += '-->\n'
        with open(self.infile, 'r') as f:
            renderedtempl = _jenv.from_string(f.read()).render()
            if self.strip_comments:
                renderedtempl = re.sub(
                    r"(<!--.*?-->)", "", renderedtempl, flags=re.DOTALL)
                renderedtempl = re.sub(
                    r"({#.*?#})", "", renderedtempl, flags=re.DOTALL)
            rendered += renderedtempl
        #if FAILED:
        #    self.failed()
        #    return
        os_utils.ensureDirExists(os.path.dirname(self.outfile), noisy=True)
        with open(self.outfile, 'w') as f:
            f.write(rendered)

opts = []
'''
NUITKA_OPTS: List[str] = [
    '--assume-yes-for-downloads',
    #'--recurse-all',
    '--follow-imports',
    '--include-package=vrctool',
    #'--plugin-enable=numpy',
    '--plugin-enable=pylint-warnings',
    '--plugin-enable=pkg-resources',
    f'--output-dir={NUITKA_OUT_DIR}',
    #'--show-progress', # *screaming*
    '--standalone',
    '--onefile' #WIP
]
'''
os_utils.ensureDirExists('archives')
with open('vrctool/consts.py.in', 'r') as f:
    #VERSION = Version('0.3.0')
    m = re.search(r"Version\('(\d+)\.(\d+)\.(\d+)(.*)'\)", f.read())
    major = m[1]
    minor = m[2]
    patch = m[3]
    build: int = 0
    if 'BUILD_NUMBER' in os.environ:
        build = int(os.environ['BUILD_NUMBER'])
    suffix = m[4]
    version = '.'.join([major, minor, patch])
if os_utils.is_windows():
    NUITKA_EXECUTABLE = NUITKA_OUT_DIR / f'{NUITKA_PKG_NAME}.exe'
    DIST_EXECUTABLE = DIST_DIR / 'vrctool.exe'
    DIST_EXECUTABLE_MANGLED = NUITKA_OUT_DIR / '__main__.exe'
    DIST_COPYTARGET = DIST_DIR / '_asyncio.dll'
else:
    NUITKA_EXECUTABLE = NUITKA_OUT_DIR / f'{NUITKA_PKG_NAME}.bin'
    DIST_EXECUTABLE = DIST_DIR / 'vrctool'
    DIST_EXECUTABLE_MANGLED = NUITKA_DIST_DIR / '__main__'
    DIST_COPYTARGET = DIST_DIR / '_asyncio.so'

REPOPUBKEY = os.environ.get('REPOPUBKEY', '')

bm = BuildMaestro()
argp = bm.build_argparser()
argp.add_argument('--version', type=str, default='nightly', help='Version to package as.')
argp.add_argument('--quick', action='store_true', default=False, help='Disable Nuitka and packaging steps.  Useful for testing and development.')
argp.add_argument('--deploy-to', dest='deploy_dir', type=str, default='archives', help='Where to place completed ZIP and TAR.GZ files.')
args = bm.parse_args(argp)

before_nuitka = []
if REPOPUBKEY != '':
    if os.path.isfile(REPOPUBKEY):
        import pgpy
        log.info('REPOPUBKEY exists!')
        key, _ = pgpy.PGPKey.from_file(REPOPUBKEY)
        assert key.is_public
        with open('vrcversiondata.json', 'r') as f:
            gamedata = json.load(f)
        vdata = [
            repr(gamedata.get('prefix', 'w_')), 
            repr(gamedata['major']), 
            repr(gamedata['minor']),
            repr(gamedata['revision']), 
            repr(gamedata['patch']),
            hex(int(gamedata['hash'], base=16))
        ]
        vdata = ', '.join(vdata)
        REPL = {
            r'@@VRCHASHES@@':    getVRCHashes(),
            r'@@MLHASHES@@':     getMLHashes(),
            r'@@CURGAMEBUILD@@': str(gamedata['build']),
            r'@@CURGAMETUPLE@@': f'({vdata})',
        }
        if not USE_BASE64:
            REPL['@@REPOPUBKEY@@'] = re.escape(repr(bytes(key)))
        else:
            REPL['@@REPOPUBKEY@@'] = encode_key(bytes(key), linelen=64, prefix=' '*26)

        before_nuitka += [
            bm.add(ReplaceTextTarget('vrctool/consts.py', 'vrctool/consts.py.in', replacements=REPL)).target,
        ]
    else:
        log.error('REPOPUBKEY is set to non-existent file!')
else:
    log.warning('REPOPUBKEY is not set.  Will not update embedded main repo key. (This is normal if you don\'t have the repo key.)')
    log.warning('If you DO want it set, set the environment variable to the full path of your PUBLIC PGP key.')
    with log.warning('LINUX:'):
        log.warning('$ REPOPUBKEY=/path/to/mykey.pub python build.py ...')
    with log.warning('WINDOWS:'):
        log.warning('> set REPOPUBKEY=/path/to/mykey.pub')
        log.warning('> python build.py ...')

if not args.quick:
    nuitka = bm.add(NuitkaTarget(entry_point=str(NUITKA_ENTRY_POINT), 
                                package_name=NUITKA_PKG_NAME, 
                                files=[str(p) for p in Path('vrctool').rglob('*.py')], 
                                single_file=True, 
                                dependencies=before_nuitka))
    nuitka.windows_company_name = 'VRCTool Contributors'
    nuitka.windows_file_description = 'VRCTool - VRChat Toolkit'
    nuitka.windows_product_name = 'VRCTool'
    nuitka.windows_file_version = (major, minor, patch, build)
    nuitka.windows_product_version = (major, minor, patch, build)
    nuitka.linux_onefile_icon = 'assets/icon.png'
    nuitka.enabled_plugins.add('anti-bloat')
    nuitka.enabled_plugins.add('pylint-warnings')


docs_build = [
    bm.add(GenDocTarget(str(DOC_SRC_DIR/ 'dist'), str(DIST_DIR), '.', 'README', dependencies=[])).target,
    bm.add(GenDocTarget(str(DOC_SRC_DIR), '.', '.', 'README', dependencies=[])).target,
]
for file in SRC_DOCS_DIR.rglob('*.tmpl.md'):
    srcdir = str(file.parent)
    destfile = (DOCS_DIR / (file.relative_to(SRC_DOCS_DIR)))
    destdir = str(file.relative_to(SRC_DOCS_DIR).parent)
    basename = destfile.stem.split('.')[0]
    print(DOC_SRC_DIR, DOCS_DIR, destdir, basename)
    if basename == 'README':
        continue
    docbuild = bm.add(GenDocTarget(str(DOC_SRC_DIR), str(DOCS_DIR), str(destdir), basename, dependencies=[]))
    docs_build += [docbuild.target]

if not args.quick:
    copy_ops=[]
    copy_ops += [
        bm.add(CopyFileTarget(str(NUITKA_EXECUTABLE), str(nuitka.executable_mangled), dependencies=[nuitka.target])).target,
        bm.add(CopyFileTarget(str(DIST_DIR / 'README.md'),    str('README.md'),       dependencies=[nuitka.target]+docs_build)).target,
        bm.add(CopyFileTarget(str(DIST_DIR / 'LICENSE'),      'LICENSE',              dependencies=[nuitka.target])).target
    ]
    archive_target = None
    ext = ''
    platform = ''
    if os_utils.is_linux():
        ext = 'tar.gz'
        platform = 'linux'
        archive_target = bm.add(MakeTarGZ(os.path.join('archives', f'vrctool-linux-amd64-{args.version}.tar.gz'), ['dist'], start='dist', dependencies=copy_ops)).target
    else:
        platform = 'windows'
        ext = 'zip'
        archive_target = bm.add(MakeZip(os.path.join('archives', f'vrctool-windows-amd64-{args.version}.zip'), ['dist'], start='dist', dependencies=copy_ops)).target
    '''
    basename = os.path.basename(archive_target)
    deploy_filename = os.path.join(deploy_dir, basename)
    deployed_archive = archive_target
    if 'BUILD_NUMBER' in os.environ:
        deployed_archive = bm.add(CopyFileTarget(os.path.join(
            'archives', f'vrctool-{platform}-amd64-latest.{ext}'), deployed_archive, dependencies=[deployed_archive])).target
    '''
bm.as_app(argp)
