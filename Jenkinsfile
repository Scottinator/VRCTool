pipeline {
  agent none
  environment {
    VIRTUAL_ENV = "${env.WORKSPACE}/.venv"
    GITLAB_ID="scottinator@gitlab" // see python-gitlab.cfg
    GITLAB_PROJ_ID="25379003"
  }
  stages {
    stage('Matrix') {
      matrix {
        axes {
          axis {
            name   'PLATFORM'
            values 'linux', 'windows'
          }
        }
        agent {
          label "$PLATFORM"
        }
        stages {
          stage('Preparation') { // for display purposes
            steps {
              script {
                // Shits itself due to race conditions. Jenkins doesn't have a "pre {}" yet.
                // Set build to pending
                //sh "gitlab  -g ${GITLAB_ID} project-commit-status create --project-id ${GITLAB_PROJ_ID} --commit-id ${GIT_COMMIT} --state pending --name ci/jenkins --description 'Jenkins build ${BUILD_NUMBER} pending'"

                String PYTHON_FILE = ''
                // Get some code from a GitHub repository
                git 'https://gitlab.com/Scottinator/VRCTool.git'
                sh 'git submodule update --init'

                // Cleanup old artifacts
                dir('archives') {
                  deleteDir()
                }

                // Setup Python
                if (isUnix()) {
                  PYTHON_FILE = sh(returnStdout: true, script: '''
                  #!/bin/bash
                  set -e
                  devtools/jenkins/findPython.sh
                  ''').trim()
                } else {
                  //PYTHON_FILE = bat(returnStdout: true, script: "@devtools/jenkins/findPython.bat").trim()
                  // Fuck it.
                  PYTHON_FILE = 'C:\\Python39\\python.exe'
                }

                // Setup venv
                if (isUnix()) {
                  sh "${PYTHON_FILE} -m venv --clear .venv"
                } else {
                  bat "${PYTHON_FILE} -m venv --clear .venv"
                }

                // Install prereqs
                if (isUnix()) {
                  sh ".venv/bin/python -m pip install -U setuptools wheel"
                  sh ".venv/bin/python -m pip install -U pip"
                  sh ".venv/bin/python -m pip install -U poetry"
                  sh ".venv/bin/poetry install --no-root"
                } else {
                  bat ".venv\\Scripts\\python.exe -m pip install -U setuptools wheel"
                  bat ".venv\\Scripts\\python.exe -m pip install -U pip"
                  bat ".venv\\Scripts\\python.exe -m pip install -U poetry"
                  bat ".venv\\Scripts\\poetry.exe install --no-root"
                }
              }
            }
          }
          stage('Build') {
            steps {
              script {
                if (isUnix()) {
                  sh """
                  export PATH=${env.WORKSPACE}/.venv/bin:\$PATH
                  .venv/bin/poetry run python build.py --no-colors --rebuild --version=ci${env.BUILD_NUMBER}
                  """
                } else {
                  bat """
                  set PATH=${env.WORKSPACE}\\.venv\\Scripts;%PATH%
                  .venv\\Scripts\\poetry.exe run python build.py --no-colors --rebuild --version=ci${env.BUILD_NUMBER}
                  """
                }
              }
            }
          }
          stage('Results') {
            steps {
              archiveArtifacts 'archives/*.zip,archives/*.tar.gz'
            }
          }
        }
        post {
          success {
            // Set build to successful
            if (isUnix()) {
              sh ".venv/bin/poetry run gitlab  -g ${GITLAB_ID} project-commit-status create --project-id ${GITLAB_PROJ_ID} --commit-id ${GIT_COMMIT} --state success --name ci/jenkins --description 'Jenkins build ${BUILD_NUMBER} passed'"
            } else {
              bat ".venv\\Scripts\\poetry.exe run gitlab  -g ${GITLAB_ID} project-commit-status create --project-id ${GITLAB_PROJ_ID} --commit-id ${GIT_COMMIT} --state success --name ci/jenkins --description 'Jenkins build ${BUILD_NUMBER} passed'"
            }
          }
          failure {
            // Set build to successful
            if (isUnix()) {
              sh ".venv/bin/poetry run gitlab  -g ${GITLAB_ID} project-commit-status create --project-id ${GITLAB_PROJ_ID} --commit-id ${GIT_COMMIT} --state failed --name ci/jenkins --description 'Jenkins build ${BUILD_NUMBER} failed'"
            } else {
              bat ".venv\\Scripts\\poetry.exe run gitlab  -g ${GITLAB_ID} project-commit-status create --project-id ${GITLAB_PROJ_ID} --commit-id ${GIT_COMMIT} --state failed --name ci/jenkins --description 'Jenkins build ${BUILD_NUMBER} failed'"
            }
          }
          always {
            script {
              dir('archives') {
                deleteDir()
              }
            }
          }
        }
      }
    }
  }
}
